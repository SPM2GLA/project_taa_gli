'use strict';

describe('Controller Tests', function() {

    describe('Enseignant Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockEnseignant, MockStage, MockFiliere, MockDipIfsic;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockEnseignant = jasmine.createSpy('MockEnseignant');
            MockStage = jasmine.createSpy('MockStage');
            MockFiliere = jasmine.createSpy('MockFiliere');
            MockDipIfsic = jasmine.createSpy('MockDipIfsic');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Enseignant': MockEnseignant,
                'Stage': MockStage,
                'Filiere': MockFiliere,
                'DipIfsic': MockDipIfsic
            };
            createController = function() {
                $injector.get('$controller')("EnseignantDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'projectTaaGliApp:enseignantUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
