'use strict';

describe('Controller Tests', function() {

    describe('Partenaire Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockPartenaire, MockStage, MockActivite, MockRegion, MockContact;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockPartenaire = jasmine.createSpy('MockPartenaire');
            MockStage = jasmine.createSpy('MockStage');
            MockActivite = jasmine.createSpy('MockActivite');
            MockRegion = jasmine.createSpy('MockRegion');
            MockContact = jasmine.createSpy('MockContact');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Partenaire': MockPartenaire,
                'Stage': MockStage,
                'Activite': MockActivite,
                'Region': MockRegion,
                'Contact': MockContact
            };
            createController = function() {
                $injector.get('$controller')("PartenaireDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'projectTaaGliApp:partenaireUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
