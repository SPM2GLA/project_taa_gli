'use strict';

describe('Controller Tests', function() {

    describe('DipIfsic Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockDipIfsic, MockEnseignant, MockStage, MockFiliere;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockDipIfsic = jasmine.createSpy('MockDipIfsic');
            MockEnseignant = jasmine.createSpy('MockEnseignant');
            MockStage = jasmine.createSpy('MockStage');
            MockFiliere = jasmine.createSpy('MockFiliere');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'DipIfsic': MockDipIfsic,
                'Enseignant': MockEnseignant,
                'Stage': MockStage,
                'Filiere': MockFiliere
            };
            createController = function() {
                $injector.get('$controller')("DipIfsicDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'projectTaaGliApp:dipIfsicUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
