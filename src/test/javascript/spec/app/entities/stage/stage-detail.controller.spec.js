'use strict';

describe('Controller Tests', function() {

    describe('Stage Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockStage, MockDipIfsic, MockPeriodeStage, MockContact, MockEtudiant, MockPartenaire, MockEnseignant, MockFonction;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockStage = jasmine.createSpy('MockStage');
            MockDipIfsic = jasmine.createSpy('MockDipIfsic');
            MockPeriodeStage = jasmine.createSpy('MockPeriodeStage');
            MockContact = jasmine.createSpy('MockContact');
            MockEtudiant = jasmine.createSpy('MockEtudiant');
            MockPartenaire = jasmine.createSpy('MockPartenaire');
            MockEnseignant = jasmine.createSpy('MockEnseignant');
            MockFonction = jasmine.createSpy('MockFonction');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Stage': MockStage,
                'DipIfsic': MockDipIfsic,
                'PeriodeStage': MockPeriodeStage,
                'Contact': MockContact,
                'Etudiant': MockEtudiant,
                'Partenaire': MockPartenaire,
                'Enseignant': MockEnseignant,
                'Fonction': MockFonction
            };
            createController = function() {
                $injector.get('$controller')("StageDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'projectTaaGliApp:stageUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
