package fr.istic.elgapiton.web.rest;

import fr.istic.elgapiton.ProjectTaaGliApp;

import fr.istic.elgapiton.domain.ModeObtention;
import fr.istic.elgapiton.repository.ModeObtentionRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ModeObtentionResource REST controller.
 *
 * @see ModeObtentionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectTaaGliApp.class)
public class ModeObtentionResourceIntTest {

    private static final String DEFAULT_OBTDESCRIPTION = "AAAAA";
    private static final String UPDATED_OBTDESCRIPTION = "BBBBB";

    @Inject
    private ModeObtentionRepository modeObtentionRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restModeObtentionMockMvc;

    private ModeObtention modeObtention;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ModeObtentionResource modeObtentionResource = new ModeObtentionResource();
        ReflectionTestUtils.setField(modeObtentionResource, "modeObtentionRepository", modeObtentionRepository);
        this.restModeObtentionMockMvc = MockMvcBuilders.standaloneSetup(modeObtentionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModeObtention createEntity(EntityManager em) {
        ModeObtention modeObtention = new ModeObtention()
                .obtdescription(DEFAULT_OBTDESCRIPTION);
        return modeObtention;
    }

    @Before
    public void initTest() {
        modeObtention = createEntity(em);
    }

    @Test
    @Transactional
    public void createModeObtention() throws Exception {
        int databaseSizeBeforeCreate = modeObtentionRepository.findAll().size();

        // Create the ModeObtention

        restModeObtentionMockMvc.perform(post("/api/mode-obtentions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(modeObtention)))
                .andExpect(status().isCreated());

        // Validate the ModeObtention in the database
        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        assertThat(modeObtentions).hasSize(databaseSizeBeforeCreate + 1);
        ModeObtention testModeObtention = modeObtentions.get(modeObtentions.size() - 1);
        assertThat(testModeObtention.getObtdescription()).isEqualTo(DEFAULT_OBTDESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllModeObtentions() throws Exception {
        // Initialize the database
        modeObtentionRepository.saveAndFlush(modeObtention);

        // Get all the modeObtentions
        restModeObtentionMockMvc.perform(get("/api/mode-obtentions?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(modeObtention.getId().intValue())))
                .andExpect(jsonPath("$.[*].obtdescription").value(hasItem(DEFAULT_OBTDESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getModeObtention() throws Exception {
        // Initialize the database
        modeObtentionRepository.saveAndFlush(modeObtention);

        // Get the modeObtention
        restModeObtentionMockMvc.perform(get("/api/mode-obtentions/{id}", modeObtention.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modeObtention.getId().intValue()))
            .andExpect(jsonPath("$.obtdescription").value(DEFAULT_OBTDESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingModeObtention() throws Exception {
        // Get the modeObtention
        restModeObtentionMockMvc.perform(get("/api/mode-obtentions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModeObtention() throws Exception {
        // Initialize the database
        modeObtentionRepository.saveAndFlush(modeObtention);
        int databaseSizeBeforeUpdate = modeObtentionRepository.findAll().size();

        // Update the modeObtention
        ModeObtention updatedModeObtention = modeObtentionRepository.findOne(modeObtention.getId());
        updatedModeObtention
                .obtdescription(UPDATED_OBTDESCRIPTION);

        restModeObtentionMockMvc.perform(put("/api/mode-obtentions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedModeObtention)))
                .andExpect(status().isOk());

        // Validate the ModeObtention in the database
        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        assertThat(modeObtentions).hasSize(databaseSizeBeforeUpdate);
        ModeObtention testModeObtention = modeObtentions.get(modeObtentions.size() - 1);
        assertThat(testModeObtention.getObtdescription()).isEqualTo(UPDATED_OBTDESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteModeObtention() throws Exception {
        // Initialize the database
        modeObtentionRepository.saveAndFlush(modeObtention);
        int databaseSizeBeforeDelete = modeObtentionRepository.findAll().size();

        // Get the modeObtention
        restModeObtentionMockMvc.perform(delete("/api/mode-obtentions/{id}", modeObtention.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ModeObtention> modeObtentions = modeObtentionRepository.findAll();
        assertThat(modeObtentions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
