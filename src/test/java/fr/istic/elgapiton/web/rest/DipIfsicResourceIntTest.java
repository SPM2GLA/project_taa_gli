package fr.istic.elgapiton.web.rest;

import fr.istic.elgapiton.ProjectTaaGliApp;

import fr.istic.elgapiton.domain.DipIfsic;
import fr.istic.elgapiton.repository.DipIfsicRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DipIfsicResource REST controller.
 *
 * @see DipIfsicResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectTaaGliApp.class)
public class DipIfsicResourceIntTest {

    private static final String DEFAULT_LIBELLE = "AAAAA";
    private static final String UPDATED_LIBELLE = "BBBBB";

    @Inject
    private DipIfsicRepository dipIfsicRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDipIfsicMockMvc;

    private DipIfsic dipIfsic;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DipIfsicResource dipIfsicResource = new DipIfsicResource();
        ReflectionTestUtils.setField(dipIfsicResource, "dipIfsicRepository", dipIfsicRepository);
        this.restDipIfsicMockMvc = MockMvcBuilders.standaloneSetup(dipIfsicResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DipIfsic createEntity(EntityManager em) {
        DipIfsic dipIfsic = new DipIfsic()
                .libelle(DEFAULT_LIBELLE);
        return dipIfsic;
    }

    @Before
    public void initTest() {
        dipIfsic = createEntity(em);
    }

    @Test
    @Transactional
    public void createDipIfsic() throws Exception {
        int databaseSizeBeforeCreate = dipIfsicRepository.findAll().size();

        // Create the DipIfsic

        restDipIfsicMockMvc.perform(post("/api/dip-ifsics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dipIfsic)))
                .andExpect(status().isCreated());

        // Validate the DipIfsic in the database
        List<DipIfsic> dipIfsics = dipIfsicRepository.findAll();
        assertThat(dipIfsics).hasSize(databaseSizeBeforeCreate + 1);
        DipIfsic testDipIfsic = dipIfsics.get(dipIfsics.size() - 1);
        assertThat(testDipIfsic.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    public void getAllDipIfsics() throws Exception {
        // Initialize the database
        dipIfsicRepository.saveAndFlush(dipIfsic);

        // Get all the dipIfsics
        restDipIfsicMockMvc.perform(get("/api/dip-ifsics?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(dipIfsic.getId().intValue())))
                .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE.toString())));
    }

    @Test
    @Transactional
    public void getDipIfsic() throws Exception {
        // Initialize the database
        dipIfsicRepository.saveAndFlush(dipIfsic);

        // Get the dipIfsic
        restDipIfsicMockMvc.perform(get("/api/dip-ifsics/{id}", dipIfsic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dipIfsic.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDipIfsic() throws Exception {
        // Get the dipIfsic
        restDipIfsicMockMvc.perform(get("/api/dip-ifsics/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDipIfsic() throws Exception {
        // Initialize the database
        dipIfsicRepository.saveAndFlush(dipIfsic);
        int databaseSizeBeforeUpdate = dipIfsicRepository.findAll().size();

        // Update the dipIfsic
        DipIfsic updatedDipIfsic = dipIfsicRepository.findOne(dipIfsic.getId());
        updatedDipIfsic
                .libelle(UPDATED_LIBELLE);

        restDipIfsicMockMvc.perform(put("/api/dip-ifsics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedDipIfsic)))
                .andExpect(status().isOk());

        // Validate the DipIfsic in the database
        List<DipIfsic> dipIfsics = dipIfsicRepository.findAll();
        assertThat(dipIfsics).hasSize(databaseSizeBeforeUpdate);
        DipIfsic testDipIfsic = dipIfsics.get(dipIfsics.size() - 1);
        assertThat(testDipIfsic.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    public void deleteDipIfsic() throws Exception {
        // Initialize the database
        dipIfsicRepository.saveAndFlush(dipIfsic);
        int databaseSizeBeforeDelete = dipIfsicRepository.findAll().size();

        // Get the dipIfsic
        restDipIfsicMockMvc.perform(delete("/api/dip-ifsics/{id}", dipIfsic.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<DipIfsic> dipIfsics = dipIfsicRepository.findAll();
        assertThat(dipIfsics).hasSize(databaseSizeBeforeDelete - 1);
    }
}
