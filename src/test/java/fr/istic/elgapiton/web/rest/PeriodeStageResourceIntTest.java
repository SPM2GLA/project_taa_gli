package fr.istic.elgapiton.web.rest;

import fr.istic.elgapiton.ProjectTaaGliApp;

import fr.istic.elgapiton.domain.PeriodeStage;
import fr.istic.elgapiton.repository.PeriodeStageRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PeriodeStageResource REST controller.
 *
 * @see PeriodeStageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectTaaGliApp.class)
public class PeriodeStageResourceIntTest {


    private static final LocalDate DEFAULT_DATEDEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATEDEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATEFIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATEFIN = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private PeriodeStageRepository periodeStageRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPeriodeStageMockMvc;

    private PeriodeStage periodeStage;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PeriodeStageResource periodeStageResource = new PeriodeStageResource();
        ReflectionTestUtils.setField(periodeStageResource, "periodeStageRepository", periodeStageRepository);
        this.restPeriodeStageMockMvc = MockMvcBuilders.standaloneSetup(periodeStageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PeriodeStage createEntity(EntityManager em) {
        PeriodeStage periodeStage = new PeriodeStage()
                .datedebut(DEFAULT_DATEDEBUT)
                .datefin(DEFAULT_DATEFIN);
        return periodeStage;
    }

    @Before
    public void initTest() {
        periodeStage = createEntity(em);
    }

    @Test
    @Transactional
    public void createPeriodeStage() throws Exception {
        int databaseSizeBeforeCreate = periodeStageRepository.findAll().size();

        // Create the PeriodeStage

        restPeriodeStageMockMvc.perform(post("/api/periode-stages")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(periodeStage)))
                .andExpect(status().isCreated());

        // Validate the PeriodeStage in the database
        List<PeriodeStage> periodeStages = periodeStageRepository.findAll();
        assertThat(periodeStages).hasSize(databaseSizeBeforeCreate + 1);
        PeriodeStage testPeriodeStage = periodeStages.get(periodeStages.size() - 1);
        assertThat(testPeriodeStage.getDatedebut()).isEqualTo(DEFAULT_DATEDEBUT);
        assertThat(testPeriodeStage.getDatefin()).isEqualTo(DEFAULT_DATEFIN);
    }

    @Test
    @Transactional
    public void getAllPeriodeStages() throws Exception {
        // Initialize the database
        periodeStageRepository.saveAndFlush(periodeStage);

        // Get all the periodeStages
        restPeriodeStageMockMvc.perform(get("/api/periode-stages?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(periodeStage.getId().intValue())))
                .andExpect(jsonPath("$.[*].datedebut").value(hasItem(DEFAULT_DATEDEBUT.toString())))
                .andExpect(jsonPath("$.[*].datefin").value(hasItem(DEFAULT_DATEFIN.toString())));
    }

    @Test
    @Transactional
    public void getPeriodeStage() throws Exception {
        // Initialize the database
        periodeStageRepository.saveAndFlush(periodeStage);

        // Get the periodeStage
        restPeriodeStageMockMvc.perform(get("/api/periode-stages/{id}", periodeStage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(periodeStage.getId().intValue()))
            .andExpect(jsonPath("$.datedebut").value(DEFAULT_DATEDEBUT.toString()))
            .andExpect(jsonPath("$.datefin").value(DEFAULT_DATEFIN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPeriodeStage() throws Exception {
        // Get the periodeStage
        restPeriodeStageMockMvc.perform(get("/api/periode-stages/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePeriodeStage() throws Exception {
        // Initialize the database
        periodeStageRepository.saveAndFlush(periodeStage);
        int databaseSizeBeforeUpdate = periodeStageRepository.findAll().size();

        // Update the periodeStage
        PeriodeStage updatedPeriodeStage = periodeStageRepository.findOne(periodeStage.getId());
        updatedPeriodeStage
                .datedebut(UPDATED_DATEDEBUT)
                .datefin(UPDATED_DATEFIN);

        restPeriodeStageMockMvc.perform(put("/api/periode-stages")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedPeriodeStage)))
                .andExpect(status().isOk());

        // Validate the PeriodeStage in the database
        List<PeriodeStage> periodeStages = periodeStageRepository.findAll();
        assertThat(periodeStages).hasSize(databaseSizeBeforeUpdate);
        PeriodeStage testPeriodeStage = periodeStages.get(periodeStages.size() - 1);
        assertThat(testPeriodeStage.getDatedebut()).isEqualTo(UPDATED_DATEDEBUT);
        assertThat(testPeriodeStage.getDatefin()).isEqualTo(UPDATED_DATEFIN);
    }

    @Test
    @Transactional
    public void deletePeriodeStage() throws Exception {
        // Initialize the database
        periodeStageRepository.saveAndFlush(periodeStage);
        int databaseSizeBeforeDelete = periodeStageRepository.findAll().size();

        // Get the periodeStage
        restPeriodeStageMockMvc.perform(delete("/api/periode-stages/{id}", periodeStage.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PeriodeStage> periodeStages = periodeStageRepository.findAll();
        assertThat(periodeStages).hasSize(databaseSizeBeforeDelete - 1);
    }
}
