package fr.istic.elgapiton.web.rest;

import fr.istic.elgapiton.ProjectTaaGliApp;

import fr.istic.elgapiton.domain.Partenaire;
import fr.istic.elgapiton.repository.PartenaireRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PartenaireResource REST controller.
 *
 * @see PartenaireResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProjectTaaGliApp.class)
public class PartenaireResourceIntTest {

    private static final String DEFAULT_NOM = "AAAAA";
    private static final String UPDATED_NOM = "BBBBB";
    private static final String DEFAULT_ADRESSE = "AAAAA";
    private static final String UPDATED_ADRESSE = "BBBBB";

    private static final Long DEFAULT_SIRET = 1L;
    private static final Long UPDATED_SIRET = 2L;

    private static final LocalDate DEFAULT_DATECREATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATECREATION = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private PartenaireRepository partenaireRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPartenaireMockMvc;

    private Partenaire partenaire;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PartenaireResource partenaireResource = new PartenaireResource();
        ReflectionTestUtils.setField(partenaireResource, "partenaireRepository", partenaireRepository);
        this.restPartenaireMockMvc = MockMvcBuilders.standaloneSetup(partenaireResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Partenaire createEntity(EntityManager em) {
        Partenaire partenaire = new Partenaire()
                .nom(DEFAULT_NOM)
                .adresse(DEFAULT_ADRESSE)
                .siret(DEFAULT_SIRET)
                .datecreation(DEFAULT_DATECREATION);
        return partenaire;
    }

    @Before
    public void initTest() {
        partenaire = createEntity(em);
    }

    @Test
    @Transactional
    public void createPartenaire() throws Exception {
        int databaseSizeBeforeCreate = partenaireRepository.findAll().size();

        // Create the Partenaire

        restPartenaireMockMvc.perform(post("/api/partenaires")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(partenaire)))
                .andExpect(status().isCreated());

        // Validate the Partenaire in the database
        List<Partenaire> partenaires = partenaireRepository.findAll();
        assertThat(partenaires).hasSize(databaseSizeBeforeCreate + 1);
        Partenaire testPartenaire = partenaires.get(partenaires.size() - 1);
        assertThat(testPartenaire.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testPartenaire.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testPartenaire.getSiret()).isEqualTo(DEFAULT_SIRET);
        assertThat(testPartenaire.getDatecreation()).isEqualTo(DEFAULT_DATECREATION);
    }

    @Test
    @Transactional
    public void getAllPartenaires() throws Exception {
        // Initialize the database
        partenaireRepository.saveAndFlush(partenaire);

        // Get all the partenaires
        restPartenaireMockMvc.perform(get("/api/partenaires?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(partenaire.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
                .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE.toString())))
                .andExpect(jsonPath("$.[*].siret").value(hasItem(DEFAULT_SIRET.intValue())))
                .andExpect(jsonPath("$.[*].datecreation").value(hasItem(DEFAULT_DATECREATION.toString())));
    }

    @Test
    @Transactional
    public void getPartenaire() throws Exception {
        // Initialize the database
        partenaireRepository.saveAndFlush(partenaire);

        // Get the partenaire
        restPartenaireMockMvc.perform(get("/api/partenaires/{id}", partenaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(partenaire.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE.toString()))
            .andExpect(jsonPath("$.siret").value(DEFAULT_SIRET.intValue()))
            .andExpect(jsonPath("$.datecreation").value(DEFAULT_DATECREATION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPartenaire() throws Exception {
        // Get the partenaire
        restPartenaireMockMvc.perform(get("/api/partenaires/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePartenaire() throws Exception {
        // Initialize the database
        partenaireRepository.saveAndFlush(partenaire);
        int databaseSizeBeforeUpdate = partenaireRepository.findAll().size();

        // Update the partenaire
        Partenaire updatedPartenaire = partenaireRepository.findOne(partenaire.getId());
        updatedPartenaire
                .nom(UPDATED_NOM)
                .adresse(UPDATED_ADRESSE)
                .siret(UPDATED_SIRET)
                .datecreation(UPDATED_DATECREATION);

        restPartenaireMockMvc.perform(put("/api/partenaires")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedPartenaire)))
                .andExpect(status().isOk());

        // Validate the Partenaire in the database
        List<Partenaire> partenaires = partenaireRepository.findAll();
        assertThat(partenaires).hasSize(databaseSizeBeforeUpdate);
        Partenaire testPartenaire = partenaires.get(partenaires.size() - 1);
        assertThat(testPartenaire.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testPartenaire.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testPartenaire.getSiret()).isEqualTo(UPDATED_SIRET);
        assertThat(testPartenaire.getDatecreation()).isEqualTo(UPDATED_DATECREATION);
    }

    @Test
    @Transactional
    public void deletePartenaire() throws Exception {
        // Initialize the database
        partenaireRepository.saveAndFlush(partenaire);
        int databaseSizeBeforeDelete = partenaireRepository.findAll().size();

        // Get the partenaire
        restPartenaireMockMvc.perform(delete("/api/partenaires/{id}", partenaire.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Partenaire> partenaires = partenaireRepository.findAll();
        assertThat(partenaires).hasSize(databaseSizeBeforeDelete - 1);
    }
}
