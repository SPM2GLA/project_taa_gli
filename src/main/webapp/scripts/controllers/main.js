'use strict';

/**
 * @ngdoc function
 * @name projectTaaGliApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the projectTaaGliApp
 */
angular.module('projectTaaGliApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
