'use strict';

/**
 * @ngdoc function
 * @name projectTaaGliApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the projectTaaGliApp
 */
angular.module('projectTaaGliApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
