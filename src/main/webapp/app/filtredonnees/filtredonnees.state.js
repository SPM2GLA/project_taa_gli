(function() { 
    'use strict'; 
 
    angular 
        .module('projectTaaGliApp') 
        .config(stateConfig); 
 
    stateConfig.$inject = ['$stateProvider']; 
 
    function stateConfig($stateProvider) { 
        $stateProvider.state('filtredonnees', { 
            parent: 'app', 
            url: '/filtredonnees', 
            data: { 
                authorities: [] 
            }, 
            views: { 
                'content@': { 
                    templateUrl: 'app/filtredonnees/filtredonnees.html', 
                    controller: 'EtudiantController', 
                    controllerAs: 'vm' 
                } 
            }, 
             
        })
    } 
})(); 