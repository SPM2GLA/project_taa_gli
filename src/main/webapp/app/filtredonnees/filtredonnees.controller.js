(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('filtredonneesController', FiltreDonneesController);

    FiltreDonneesController.$inject = ['$scope', '$state', 'Etudiant'];

    function FiltreDonneesController ($scope, $state, Etudiant) {
        var vm = this;
        
        vm.etudiants = [];

        loadAll();

        function loadAll() {
            Contact.query(function(result) {
                vm.etudiants = result;
            });
        }
    }
})();