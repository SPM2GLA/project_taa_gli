(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('EtudiantInfoDialogController', EtudiantInfoDialogController);

    EtudiantInfoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Etudiant', 'Stage', 'Enquete', 'User'];

    function EtudiantInfoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Etudiant, Stage, Enquete, User) {
        var vm = this;

        vm.etudiant = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.stages = Stage.query();
        vm.enquetes = Enquete.query();
        vm.users = User.query();

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.etudiant.id !== null) {
                Etudiant.update(vm.etudiant, onSaveSuccess, onSaveError);
            } else {
                Etudiant.save(vm.etudiant, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('projectTaaGliApp:etudiantUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datenaissance = false;

        function openCalendar(date) {
          console.log("calendar app");
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
