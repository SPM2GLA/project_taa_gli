(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('etudiantInfoController', EtudiantInfoController);

    EtudiantInfoController.$inject = ['$scope', 'Principal', '$state', 'Etudiant'];

    function EtudiantInfoController($scope, Principal, $state, Etudiant) {
        var vm = this;
        vm.etudiant = null;

        vm.etudiants = [];
        loadAll();
        console.log("data promise may caause an error");
        function loadAll() {
            console.log("Bonjour");

            Etudiant.query(function(result) {
                vm.etudiants = result;

                vm.etudiant = vm.etudiants[0];
                console.log(vm.etudiant);
            });
        }
    }
})();
