(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('etudiantinfo', {
            parent: 'app',
            url: '/etudiantinfo',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.etudiantinfo.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/etudiantinfo/etudiantinfo.html',
                    controller: 'etudiantInfoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('etudiant');
                    $translatePartialLoader.addPart('etudiantinfo');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('etudiantinfo.edit', {
            parent: 'etudiantinfo',
            url: '/{id}/edit',
            ata: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/etudiantinfo/etudiantinfo-dialog.html',
                    controller: 'EtudiantInfoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Etudiant', function(Etudiant) {
                            return Etudiant.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('etudiant', null, { reload: 'etudiant' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
