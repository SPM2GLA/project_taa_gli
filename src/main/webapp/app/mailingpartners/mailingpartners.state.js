(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('mailingpartners', {
            parent: 'app',
            url: '/mailing',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/mailingpartners/mailingpartners.html',
                    controller: 'ShowStagesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('stage');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }

        })
        .state('sendmail', {
            parent: 'app',
            url: '/sendmail',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/mailingpartners/sendmail.html',
                    controller: 'ContactController',
                    controllerAs: 'vm'
                }

           },
           });
    }
})();
