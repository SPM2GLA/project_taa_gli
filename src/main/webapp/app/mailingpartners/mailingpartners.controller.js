(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ShowStagesController', ShowStagesController);

    ShowStagesController.$inject = ['$scope', '$state', 'Stage'];

    function ShowStagesController ($scope, $state, Stage) {
        var vm = this;

        vm.stages = [];

        loadAll();

        function loadAll() {
          Stage.query(function(result) {
                vm.stages = result;
            });
        }
    }
})();
