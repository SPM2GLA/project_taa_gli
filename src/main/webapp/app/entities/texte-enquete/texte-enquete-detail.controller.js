(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('TexteEnqueteDetailController', TexteEnqueteDetailController);

    TexteEnqueteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TexteEnquete', 'Enquete', 'Etudiant'];

    function TexteEnqueteDetailController($scope, $rootScope, $stateParams, previousState, entity, TexteEnquete, Enquete, Etudiant) {
        var vm = this;

        vm.texteEnquete = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('projectTaaGliApp:texteEnqueteUpdate', function(event, result) {
            vm.texteEnquete = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
