(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('TexteEnqueteController', TexteEnqueteController);

    TexteEnqueteController.$inject = ['$scope', '$state', 'TexteEnquete', 'Csv'];

    function TexteEnqueteController ($scope, $state, TexteEnquete, Csv) {
        var vm = this;

        vm.generateCSV = generateCSV;
        vm.texteEnquetes = [];

        loadAll();

        function loadAll() {
            TexteEnquete.query(function(result) {
                vm.texteEnquetes = result;
            });
        }

        function generateCSV() {
          var idList =[];
          for (var i = 0; i < vm.filteredStage.length; i++) {
              idList.push(vm.filteredStage[i].id);
              //recupération dans idList des différentes id&filtre pour la requete rest
          }
          console.log("call downloadCsv");
            var fileName = "TexteEnquete.csv";
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            Csv.downloadCsvtexteenquete(idList).then(function(result) {
              console.log("call callback");
              console.log(result);
              console.log(result.data.byteLength);
              if(result.data.byteLength === 0)
              alert("erreur generation csv");
              else{

                var file = new Blob([result.data], {
                    type: 'application/csv'
                });
                var fileURL = window.URL.createObjectURL(file);
                a.href = fileURL;
                a.download = fileName;
                a.click();
              }
            });
        }
    }
})();
