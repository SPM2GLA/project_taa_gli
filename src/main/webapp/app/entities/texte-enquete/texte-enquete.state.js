(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('texte-enquete', {
            parent: 'entity',
            url: '/texte-enquete',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.texteEnquete.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/texte-enquete/texte-enquetes.html',
                    controller: 'TexteEnqueteController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('texteEnquete');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('texte-enquete-detail', {
            parent: 'entity',
            url: '/texte-enquete/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.texteEnquete.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-detail.html',
                    controller: 'TexteEnqueteDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('texteEnquete');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TexteEnquete', function($stateParams, TexteEnquete) {
                    return TexteEnquete.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'texte-enquete',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('texte-enquete-detail.edit', {
            parent: 'texte-enquete-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-dialog.html',
                    controller: 'TexteEnqueteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TexteEnquete', function(TexteEnquete) {
                            return TexteEnquete.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('texte-enquete.new', {
            parent: 'texte-enquete',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-dialog.html',
                    controller: 'TexteEnqueteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                reponse: null,
                                question: null,
                                dureerecherche: null,
                                salaire: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('texte-enquete', null, { reload: 'texte-enquete' });
                }, function() {
                    $state.go('texte-enquete');
                });
            }]
        })
        .state('texte-enquete.edit', {
            parent: 'texte-enquete',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-dialog.html',
                    controller: 'TexteEnqueteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TexteEnquete', function(TexteEnquete) {
                            return TexteEnquete.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('texte-enquete', null, { reload: 'texte-enquete' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('texte-enquete.delete', {
            parent: 'texte-enquete',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/texte-enquete/texte-enquete-delete-dialog.html',
                    controller: 'TexteEnqueteDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TexteEnquete', function(TexteEnquete) {
                            return TexteEnquete.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('texte-enquete', null, { reload: 'texte-enquete' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
