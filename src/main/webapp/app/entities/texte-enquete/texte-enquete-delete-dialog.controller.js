(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('TexteEnqueteDeleteController',TexteEnqueteDeleteController);

    TexteEnqueteDeleteController.$inject = ['$uibModalInstance', 'entity', 'TexteEnquete'];

    function TexteEnqueteDeleteController($uibModalInstance, entity, TexteEnquete) {
        var vm = this;

        vm.texteEnquete = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TexteEnquete.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
