(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('TexteEnquete', TexteEnquete);

    TexteEnquete.$inject = ['$resource'];

    function TexteEnquete ($resource) {
        var resourceUrl =  'api/texte-enquetes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
