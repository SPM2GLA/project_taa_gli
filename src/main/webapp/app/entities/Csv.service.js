(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('Csv', Csv);

    Csv.$inject = ['$http'];

    function Csv($http) {

        return {
          downloadCsvactivite: function(ids) {
              return $http.get('api/activite/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvcontact: function(ids) {
              return $http.get('api/contact/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvdipifsic: function(ids) {
              return $http.get('api/dip-ifsic/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvenquete: function(ids) {
              return $http.get('api/enquete/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvenseignant: function(ids) {
              return $http.get('api/enseignant/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvetudiant: function(ids) {
              return $http.get('api/etudiant/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvfiliere: function(ids) {
              return $http.get('api/filiere/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvfonction: function(ids) {
              return $http.get('api/fonction/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvmodeobtention: function(ids) {
              return $http.get('api/mode-obtention/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvpartenaire: function(ids) {
              return $http.get('api/partenaire/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvperiodestage: function(ids) {
              return $http.get('api/periode-stage/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvregion: function(ids) {
              return $http.get('api/region/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          },
          downloadCsvtexteenquete: function(ids) {
              return $http.get('api/texte-enquete/csv/'+ids, {
                isArray: true,
                  responseType: 'arraybuffer'
              }).then(
                  function(response) {
                      return response;
                  });
          }
        };

    }
})();
