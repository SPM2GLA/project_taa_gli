(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('Stage', Stage);

    Stage.$inject = ['$resource', 'DateUtils'];

    function Stage ($resource, DateUtils) {
        var resourceUrl =  'api/stages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.sannee = DateUtils.convertLocalDateFromServer(data.sannee);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.sannee = DateUtils.convertLocalDateToServer(copy.sannee);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.sannee = DateUtils.convertLocalDateToServer(copy.sannee);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
