(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('StagePartenaire',StagePartenaire);

    StagePartenaire.$inject = ['$resource'];

      function StagePartenaire ($resource) {
           var resourceUrl =  'api/stages';
           return $resource(resourceUrl, {}, {
               'query': { method: 'GET', isArray: true},

                'update': { method:'PUT' }

           });

      }
})();
