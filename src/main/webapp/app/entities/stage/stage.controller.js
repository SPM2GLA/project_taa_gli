(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('StageController', StageController);

    StageController.$inject = ['$scope', '$state', 'Stage', 'StageInfo'];

    function StageController ($scope, $state, Stage, StageInfo) {
        var vm = this;

        vm.stages = [];
        vm.CSV = CSV;
        loadAll();

        function loadAll() {
            Stage.query(function(result) {
                vm.stages = result;
            });
        }
        function CSV() {
          var idList =[];
          for (var i = 0; i < vm.filteredStage.length; i++) {
              idList.push(vm.filteredStage[i].id);
              //recupération dans idList des différentes id&filtre pour la requete rest
          }
          console.log("call downloadCsv");
            var fileName = "Stage.csv";
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            StageInfo.downloadCsv(idList).then(function(result) {
              console.log("call callback");
              console.log(result);
              console.log(result.data.byteLength);
              if(result.data.byteLength === 0)
              alert("erreur generation csv");
              else{

                var file = new Blob([result.data], {
                    type: 'application/csv'
                });
                var fileURL = window.URL.createObjectURL(file);
                a.href = fileURL;
                a.download = fileName;
                a.click();
              }
            });
        }
    }
})();
