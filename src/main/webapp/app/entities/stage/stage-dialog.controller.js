(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('StageDialogController', StageDialogController);


    StageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Stage', 'DipIfsic', 'PeriodeStage', 'Contact', 'Etudiant', 'Partenaire', 'Enseignant', 'Fonction'];

    function StageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Stage, DipIfsic, PeriodeStage, Contact, Etudiant, Partenaire, Enseignant, Fonction) {
       var vm = this;

        vm.stage = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.diplomeifsics = DipIfsic.query({filter: 'stage-is-null'});
        $q.all([vm.stage.$promise, vm.diplomeifsics.$promise]).then(function() {
            if (!vm.stage.diplomeifsic || !vm.stage.diplomeifsic.id) {
                return $q.reject();
            }
            return DipIfsic.get({id : vm.stage.diplomeifsic.id}).$promise;
        }).then(function(diplomeifsic) {
            vm.diplomeifsics.push(diplomeifsic);
        });
        vm.periodestages = PeriodeStage.query({filter: 'stage-is-null'});
        $q.all([vm.stage.$promise, vm.periodestages.$promise]).then(function() {
            if (!vm.stage.periodestage || !vm.stage.periodestage.id) {
                return $q.reject();
            }
            return PeriodeStage.get({id : vm.stage.periodestage.id}).$promise;
        }).then(function(periodestage) {
            vm.periodestages.push(periodestage);
        });
        vm.contacts = Contact.query();
        vm.etudiants = Etudiant.query();
        vm.partenaires = Partenaire.query();
        vm.enseignants = Enseignant.query();
        vm.fonctions = Fonction.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
var save;
        function save () {
            vm.isSaving = true;
            if (vm.stage.id !== null) {
                Stage.update(vm.stage, onSaveSuccess, onSaveError);
            } else {
              save.contact = vm.stage.contacts;
                vm.stage.contact = null;
                save.fonction = vm.stage.fonction;
                  vm.stage.fonction = null;
                  save.diplomeifsic = vm.stage.diplomeifsic;
                    vm.stage.diplomeifsic = null;
                    save.periodestage = vm.stage.periodestage;
                      vm.stage.periodestage = null;
                Stage.save(vm.stage, onSaveSuccess1, onSaveError);
            }
        }

        function onSaveSuccess1 (result) {
          vm.stage = result;
          vm.stage.contacts = save.contact;
          vm.stage.fonction = save.fonction;
          vm.stage.diplomeifsic = save.diplomeifsic;
          vm.stage.periodestage = save.periodestage;
          vm.isSaving = true;
          if (vm.stage.id !== null) {
              Stage.update(vm.stage, onSaveSuccess, onSaveError);
          } else {

              Stage.save(vm.stage, onSaveSuccess1, onSaveError);
          }
        }

        function onSaveSuccess (result) {
            $scope.$emit('projectTaaGliApp:stageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.sannee = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }

        $scope.datepickerOptions = {
            datepickerMode: "year",
            minMode: "year",
            minDate: "minDate",
            showWeeks: "false",
        };

        $scope.getPeriodeLabel = function(periode){
//        	console.log(periode);
        	return periode.datedebut + " - " +periode.datefin;
//        	return $filter('date')(periode.datedebut)+" - "+$filter('date')(periode.datefin)
        }
//        $scope.date = new Date();
    }
})();
