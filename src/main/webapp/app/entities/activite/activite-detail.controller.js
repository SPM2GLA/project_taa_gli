(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ActiviteDetailController', ActiviteDetailController);

    ActiviteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Activite', 'Partenaire'];

    function ActiviteDetailController($scope, $rootScope, $stateParams, previousState, entity, Activite, Partenaire) {
        var vm = this;

        vm.activite = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('projectTaaGliApp:activiteUpdate', function(event, result) {
            vm.activite = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
