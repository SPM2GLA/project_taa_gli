(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ActiviteDeleteController',ActiviteDeleteController);

    ActiviteDeleteController.$inject = ['$uibModalInstance', 'entity', 'Activite'];

    function ActiviteDeleteController($uibModalInstance, entity, Activite) {
        var vm = this;

        vm.activite = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Activite.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
