(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ActiviteDialogController', ActiviteDialogController);

    ActiviteDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Activite', 'Partenaire'];

    function ActiviteDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Activite, Partenaire) {
        var vm = this;

        vm.activite = entity;
        vm.clear = clear;
        vm.save = save;
        vm.partenaires = Partenaire.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.activite.id !== null) {
                Activite.update(vm.activite, onSaveSuccess, onSaveError);
            } else {
                Activite.save(vm.activite, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('projectTaaGliApp:activiteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
