(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ContactController', ContactController);

    ContactController.$inject = ['$scope', '$log','$state', 'Contact', 'Csv'];

    function ContactController ($scope, $state, $log,Contact, Csv) {
        var vm = this;

        vm.generateCSV = generateCSV;
        vm.contacts = [];

        loadAll();

        function loadAll() {
            Contact.query(function(result) {
                vm.contacts = result;
            });
        }

       $scope.$watch('selectedcontacts', function() {


       console.log($scope.selectedcontacts);

   });

   $scope.send=function(){

   var link = "mailto:"+ $scope.selectedcontacts +"?subject=informations%20sur%20le%20stage ";

    window.location.href = link;

   }

   function generateCSV() {
     console.log("click");
     var idList =[];
     for (var i = 0; i < vm.filteredStage.length; i++) {
         idList.push(vm.filteredStage[i].id);
         //recupération dans idList des différentes id&filtre pour la requete rest
     }
     console.log(idList);
     console.log("call downloadCsv");
       var fileName = "Contact.csv";
       var a = document.createElement("a");
       document.body.appendChild(a);
       a.style = "display: none";
       Csv.downloadCsvcontact(idList).then(function(result) {
         console.log("call callback");
         console.log(result);
         console.log(result.data.byteLength);
         if(result.data.byteLength === 0)
         alert("erreur generation csv");
         else{

           var file = new Blob([result.data], {
               type: 'application/csv'
           });
           var fileURL = window.URL.createObjectURL(file);
           a.href = fileURL;
           a.download = fileName;
           a.click();
         }
       });
   }
    }
})();
