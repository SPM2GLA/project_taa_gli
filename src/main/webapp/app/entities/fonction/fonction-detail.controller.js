(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('FonctionDetailController', FonctionDetailController);

    FonctionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Fonction', 'Stage'];

    function FonctionDetailController($scope, $rootScope, $stateParams, previousState, entity, Fonction, Stage) {
        var vm = this;

        vm.fonction = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('projectTaaGliApp:fonctionUpdate', function(event, result) {
            vm.fonction = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
