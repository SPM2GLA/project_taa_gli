(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('fonction', {
            parent: 'entity',
            url: '/fonction',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.fonction.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/fonction/fonctions.html',
                    controller: 'FonctionController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('fonction');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('fonction-detail', {
            parent: 'entity',
            url: '/fonction/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.fonction.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/fonction/fonction-detail.html',
                    controller: 'FonctionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('fonction');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Fonction', function($stateParams, Fonction) {
                    return Fonction.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'fonction',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('fonction-detail.edit', {
            parent: 'fonction-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/fonction/fonction-dialog.html',
                    controller: 'FonctionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Fonction', function(Fonction) {
                            return Fonction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('fonction.new', {
            parent: 'fonction',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/fonction/fonction-dialog.html',
                    controller: 'FonctionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                libelle: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('fonction', null, { reload: 'fonction' });
                }, function() {
                    $state.go('fonction');
                });
            }]
        })
        .state('fonction.edit', {
            parent: 'fonction',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/fonction/fonction-dialog.html',
                    controller: 'FonctionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Fonction', function(Fonction) {
                            return Fonction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('fonction', null, { reload: 'fonction' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('fonction.delete', {
            parent: 'fonction',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/fonction/fonction-delete-dialog.html',
                    controller: 'FonctionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Fonction', function(Fonction) {
                            return Fonction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('fonction', null, { reload: 'fonction' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
