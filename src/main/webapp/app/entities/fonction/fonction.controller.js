(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('FonctionController', FonctionController);

    FonctionController.$inject = ['$scope', '$state', 'Fonction', 'Csv'];

    function FonctionController ($scope, $state, Fonction, Csv) {
        var vm = this;

        vm.generateCSV = generateCSV;
        vm.fonctions = [];

        loadAll();

        function loadAll() {
            Fonction.query(function(result) {
                vm.fonctions = result;
            });
        }

        function generateCSV() {
          var idList =[];
          for (var i = 0; i < vm.filteredStage.length; i++) {
              idList.push(vm.filteredStage[i].id);
              //recupération dans idList des différentes id&filtre pour la requete rest
          }
          console.log("call downloadCsv");
            var fileName = "Fonction.csv";
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            Csv.downloadCsvfonction(idList).then(function(result) {
              console.log("call callback");
              console.log(result);
              console.log(result.data.byteLength);
              if(result.data.byteLength === 0)
              alert("erreur generation csv");
              else{

                var file = new Blob([result.data], {
                    type: 'application/csv'
                });
                var fileURL = window.URL.createObjectURL(file);
                a.href = fileURL;
                a.download = fileName;
                a.click();
              }
            });
        }
    }
})();
