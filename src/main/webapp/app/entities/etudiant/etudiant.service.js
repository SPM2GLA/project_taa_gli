(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('Etudiant', Etudiant);

    Etudiant.$inject = ['$resource', 'DateUtils'];

    function Etudiant ($resource, DateUtils) {
        var resourceUrl =  'api/etudiants/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.datenaissance = DateUtils.convertLocalDateFromServer(data.datenaissance);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.datenaissance = DateUtils.convertLocalDateToServer(copy.datenaissance);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.datenaissance = DateUtils.convertLocalDateToServer(copy.datenaissance);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
