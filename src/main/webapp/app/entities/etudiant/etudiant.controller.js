(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('EtudiantController', EtudiantController);

    EtudiantController.$inject = ['$scope','$state','Etudiant', 'Csv'];

    function EtudiantController ($scope,$state, Etudiant, Csv) {
   var vm = this;
   vm.etudiants=[];

   vm.generateCSV = generateCSV;


        loadAll();

       function loadAll() {
            Etudiant.query(function(result) {
                vm.etudiants = result;

            });
        }

        function generateCSV() {
          var idList =[];
          for (var i = 0; i < vm.filteredStage.length; i++) {
              idList.push(vm.filteredStage[i].id);
              //recupération dans idList des différentes id&filtre pour la requete rest
          }
          console.log("call downloadCsv");
            var fileName = "Etudiant.csv";
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            Csv.downloadCsvetudiant(idList).then(function(result) {
              console.log("call callback");
              console.log(result);
              console.log(result.data.byteLength);
              if(result.data.byteLength === 0)
              alert("erreur generation csv");
              else{

                var file = new Blob([result.data], {
                    type: 'application/csv'
                });
                var fileURL = window.URL.createObjectURL(file);
                a.href = fileURL;
                a.download = fileName;
                a.click();
              }
            });
        }
      }



})();
