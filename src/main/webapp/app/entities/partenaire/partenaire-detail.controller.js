(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('PartenaireDetailController', PartenaireDetailController);

    PartenaireDetailController.$inject = ['$scope','sharedProperties', '$rootScope', '$stateParams', 'previousState', 'entity', 'Partenaire', 'Stage', 'Activite', 'Region', 'Contact'];

    function PartenaireDetailController($scope,sharedProperties,$rootScope, $stateParams, previousState, entity, Partenaire, Stage, Activite, Region, Contact) {
        var vm = this;

        vm.partenaire = entity;
        vm.partner=vm.partenaire;
        console.log(vm.partner.nom);
        vm.previousState = previousState.name;
        vm.stage={};
        vm.save=save;

        var unsubscribe = $rootScope.$on('projectTaaGliApp:partenaireUpdate', function(event, result) {
            vm.partenaire = result;
        });
        $scope.$on('$destroy', unsubscribe);

        function save () {
           console.log("save methode partenaire dans stage called")
             vm.stage=sharedProperties.getProperty();
             vm.stage.partenaire=vm.partenaire;
             Stage.update(vm.stage);

            }

    }
})();
