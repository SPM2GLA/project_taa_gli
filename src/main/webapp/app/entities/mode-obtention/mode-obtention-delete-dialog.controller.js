(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ModeObtentionDeleteController',ModeObtentionDeleteController);

    ModeObtentionDeleteController.$inject = ['$uibModalInstance', 'entity', 'ModeObtention'];

    function ModeObtentionDeleteController($uibModalInstance, entity, ModeObtention) {
        var vm = this;

        vm.modeObtention = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ModeObtention.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
