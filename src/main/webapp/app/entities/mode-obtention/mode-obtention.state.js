(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('mode-obtention', {
            parent: 'entity',
            url: '/mode-obtention',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.modeObtention.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/mode-obtention/mode-obtentions.html',
                    controller: 'ModeObtentionController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('modeObtention');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('mode-obtention-detail', {
            parent: 'entity',
            url: '/mode-obtention/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.modeObtention.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/mode-obtention/mode-obtention-detail.html',
                    controller: 'ModeObtentionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('modeObtention');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ModeObtention', function($stateParams, ModeObtention) {
                    return ModeObtention.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'mode-obtention',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('mode-obtention-detail.edit', {
            parent: 'mode-obtention-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mode-obtention/mode-obtention-dialog.html',
                    controller: 'ModeObtentionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ModeObtention', function(ModeObtention) {
                            return ModeObtention.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('mode-obtention.new', {
            parent: 'mode-obtention',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mode-obtention/mode-obtention-dialog.html',
                    controller: 'ModeObtentionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                obtdescription: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('mode-obtention', null, { reload: 'mode-obtention' });
                }, function() {
                    $state.go('mode-obtention');
                });
            }]
        })
        .state('mode-obtention.edit', {
            parent: 'mode-obtention',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mode-obtention/mode-obtention-dialog.html',
                    controller: 'ModeObtentionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ModeObtention', function(ModeObtention) {
                            return ModeObtention.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('mode-obtention', null, { reload: 'mode-obtention' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('mode-obtention.delete', {
            parent: 'mode-obtention',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mode-obtention/mode-obtention-delete-dialog.html',
                    controller: 'ModeObtentionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ModeObtention', function(ModeObtention) {
                            return ModeObtention.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('mode-obtention', null, { reload: 'mode-obtention' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
