(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ModeObtentionDetailController', ModeObtentionDetailController);

    ModeObtentionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ModeObtention', 'Enquete'];

    function ModeObtentionDetailController($scope, $rootScope, $stateParams, previousState, entity, ModeObtention, Enquete) {
        var vm = this;

        vm.modeObtention = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('projectTaaGliApp:modeObtentionUpdate', function(event, result) {
            vm.modeObtention = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
