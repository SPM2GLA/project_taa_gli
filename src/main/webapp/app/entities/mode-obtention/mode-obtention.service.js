(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('ModeObtention', ModeObtention);

    ModeObtention.$inject = ['$resource'];

    function ModeObtention ($resource) {
        var resourceUrl =  'api/mode-obtentions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
