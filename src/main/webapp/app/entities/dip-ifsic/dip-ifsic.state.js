(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('dip-ifsic', {
            parent: 'entity',
            url: '/dip-ifsic',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.dipIfsic.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dip-ifsic/dip-ifsics.html',
                    controller: 'DipIfsicController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dipIfsic');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('dip-ifsic-detail', {
            parent: 'entity',
            url: '/dip-ifsic/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.dipIfsic.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dip-ifsic/dip-ifsic-detail.html',
                    controller: 'DipIfsicDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dipIfsic');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DipIfsic', function($stateParams, DipIfsic) {
                    return DipIfsic.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'dip-ifsic',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('dip-ifsic-detail.edit', {
            parent: 'dip-ifsic-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dip-ifsic/dip-ifsic-dialog.html',
                    controller: 'DipIfsicDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DipIfsic', function(DipIfsic) {
                            return DipIfsic.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dip-ifsic.new', {
            parent: 'dip-ifsic',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dip-ifsic/dip-ifsic-dialog.html',
                    controller: 'DipIfsicDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                libelle: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('dip-ifsic', null, { reload: 'dip-ifsic' });
                }, function() {
                    $state.go('dip-ifsic');
                });
            }]
        })
        .state('dip-ifsic.edit', {
            parent: 'dip-ifsic',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dip-ifsic/dip-ifsic-dialog.html',
                    controller: 'DipIfsicDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DipIfsic', function(DipIfsic) {
                            return DipIfsic.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dip-ifsic', null, { reload: 'dip-ifsic' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dip-ifsic.delete', {
            parent: 'dip-ifsic',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dip-ifsic/dip-ifsic-delete-dialog.html',
                    controller: 'DipIfsicDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DipIfsic', function(DipIfsic) {
                            return DipIfsic.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dip-ifsic', null, { reload: 'dip-ifsic' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
