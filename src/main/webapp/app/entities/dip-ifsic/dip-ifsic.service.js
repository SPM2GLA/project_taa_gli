(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('DipIfsic', DipIfsic);

    DipIfsic.$inject = ['$resource'];

    function DipIfsic ($resource) {
        var resourceUrl =  'api/dip-ifsics/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
