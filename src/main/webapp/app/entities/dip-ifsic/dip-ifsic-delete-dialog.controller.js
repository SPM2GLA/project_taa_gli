(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('DipIfsicDeleteController',DipIfsicDeleteController);

    DipIfsicDeleteController.$inject = ['$uibModalInstance', 'entity', 'DipIfsic'];

    function DipIfsicDeleteController($uibModalInstance, entity, DipIfsic) {
        var vm = this;

        vm.dipIfsic = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DipIfsic.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
