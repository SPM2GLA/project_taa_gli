(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('DipIfsicDialogController', DipIfsicDialogController);

    DipIfsicDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DipIfsic', 'Enseignant', 'Stage', 'Filiere'];

    function DipIfsicDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DipIfsic, Enseignant, Stage, Filiere) {
        var vm = this;

        vm.dipIfsic = entity;
        vm.clear = clear;
        vm.save = save;
        vm.enseignants = Enseignant.query();
        vm.stages = Stage.query();
        vm.filieres = Filiere.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dipIfsic.id !== null) {
                DipIfsic.update(vm.dipIfsic, onSaveSuccess, onSaveError);
            } else {
                DipIfsic.save(vm.dipIfsic, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('projectTaaGliApp:dipIfsicUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
