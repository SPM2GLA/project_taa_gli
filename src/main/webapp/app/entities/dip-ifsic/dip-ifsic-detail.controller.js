(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('DipIfsicDetailController', DipIfsicDetailController);

    DipIfsicDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DipIfsic', 'Enseignant', 'Stage', 'Filiere'];

    function DipIfsicDetailController($scope, $rootScope, $stateParams, previousState, entity, DipIfsic, Enseignant, Stage, Filiere) {
        var vm = this;

        vm.dipIfsic = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('projectTaaGliApp:dipIfsicUpdate', function(event, result) {
            vm.dipIfsic = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
