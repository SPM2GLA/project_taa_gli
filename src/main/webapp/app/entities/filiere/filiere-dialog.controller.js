(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('FiliereDialogController', FiliereDialogController);

    FiliereDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Filiere', 'DipIfsic', 'Enseignant'];

    function FiliereDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Filiere, DipIfsic, Enseignant) {
        var vm = this;

        vm.filiere = entity;
        vm.clear = clear;
        vm.save = save;
        vm.dipifsics = DipIfsic.query();
        vm.enseignants = Enseignant.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.filiere.id !== null) {
                Filiere.update(vm.filiere, onSaveSuccess, onSaveError);
            } else {
                Filiere.save(vm.filiere, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('projectTaaGliApp:filiereUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
