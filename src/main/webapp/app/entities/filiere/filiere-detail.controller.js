(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('FiliereDetailController', FiliereDetailController);

    FiliereDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Filiere', 'DipIfsic', 'Enseignant'];

    function FiliereDetailController($scope, $rootScope, $stateParams, previousState, entity, Filiere, DipIfsic, Enseignant) {
        var vm = this;

        vm.filiere = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('projectTaaGliApp:filiereUpdate', function(event, result) {
            vm.filiere = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
