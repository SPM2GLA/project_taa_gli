(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('StageInfoController', StageInfoController);

    StageInfoController.$inject = ['$scope', 'Principal', '$state', 'Stage', 'StageInfo'];

    function StageInfoController($scope, Principal, $state, Stage, StageInfo) {
        var vm = this;

        vm.stages = [];
        loadAll();

        function loadAll() {
            Stage.query(function(result) {
                vm.stages = result;
            });
        }
        vm.downloadPdf = function(id) {
          console.log("call downloadPdf");
            var fileName = "convention.pdf";
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";
            StageInfo.downloadPdf(id).then(function(result) {
              console.log("call callback");
              console.log(result);
              console.log(result.data.byteLength);
              if(result.data.byteLength === 0)
              alert("erreur generation pdf");
              else{

                var file = new Blob([result.data], {
                    type: 'application/pdf'
                });
                var fileURL = window.URL.createObjectURL(file);
                a.href = fileURL;
                a.download = fileName;
                a.click();
              }
            });
        };
    }
})();
