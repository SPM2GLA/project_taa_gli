(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('StageInfo', StageInfo);

    StageInfo.$inject = ['$http'];

    function StageInfo($http) {

        return {
            downloadPdf: function(id) {
                return $http.get('api/stage/pdf/'+id, {
                    responseType: 'arraybuffer'
                }).then(
                    function(response) {
                        return response;
                    });
            },
            downloadCsv: function(ids) {
                return $http.get('api/stage/csv/'+ids, {
                  isArray: true,
                    responseType: 'arraybuffer'
                }).then(
                    function(response) {
                        return response;
                    });
            }
        };

    }
})();
