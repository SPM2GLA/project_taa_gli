(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
      .state('stageinfo', {
          parent: 'app',
          url: '/stageinfo',
          data: {
              authorities: ['ROLE_USER'],
              pageTitle: 'projectTaaGliApp.stageinfo.title'
          },
          views: {
              'content@': {
                  templateUrl: 'app/stageinfo/stageinfo.html',
                  controller: 'StageInfoController',
                  controllerAs: 'vm'
              }
          },
          resolve: {
              translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                  $translatePartialLoader.addPart('stage');
                  $translatePartialLoader.addPart('stageinfo');
                  $translatePartialLoader.addPart('global');
                  return $translate.refresh();
              }]
          }
      });

    }

})();
