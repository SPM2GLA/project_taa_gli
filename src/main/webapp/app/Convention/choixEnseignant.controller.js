(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('choixEnseignantController', choixEnseignantController);

    choixEnseignantController.$inject = ['$scope', '$state', 'Enseignant','Stage','sharedProperties','$log'];

    function choixEnseignantController ($scope, $state, Enseignant,Stage,sharedProperties,$log) {
        var vm = this;

        vm.enseignants = [];
        vm.save=save;
        vm.var={};

        loadAll();

        function loadAll() {
            Enseignant.query(function(result) {
                vm.enseignants = result;
            });
        }

        function save(){
          console.log("appel au enregistrement de l'enseigant");
          vm.stage=sharedProperties.getProperty();
          console.log(vm.stage);
          vm.stage.enseignant=vm.var;
          Stage.update(vm.stage);
          console.log(vm.stage);

        }
    }
})();
