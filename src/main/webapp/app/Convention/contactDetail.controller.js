(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ContactDetController', ContactDetController);

    ContactDetController.$inject = ['$scope', '$stateParams', 'entity', 'Contact','sharedProperties','Stage'];

    function ContactDetController($scope,$stateParams, entity, Contact,sharedProperties,Stage) {
        var vm = this;

        vm.contact = entity;
        vm.stage={};
        vm.save=save;


        function save () {
           console.log("appel du save du contact dans stage")
             vm.stage=sharedProperties.getProperty();
             vm.stage.contacts.push(vm.contact);
             Stage.update(vm.stage);

            }


    }
})();
