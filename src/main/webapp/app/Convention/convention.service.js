//global service managing stage in all creation steps
(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('ConventionService', ConventionService);
    //déclaration du nom du service
    ConventionService.$inject = ['Etudiant', 'Stage'];

    function ConventionService(Etudiant, Stage) {
        var vm = this;
        //liste des méthodes a appeler pour get et set les différents paramètres a retenir entre les sessions
        return {
            getCurrentEtudiant: function() {
                if (vm.currentEtudiant) {
                    console.log('already etudiant');
                    console.log(vm);
                    return vm.currentEtudiant;
                } else {
                    console.log('load etudaiant');
                    return Etudiant.query(function(result) {
                        vm.currentEtudiant = result[0];
                        return vm.currentEtudiant;
                    });
                }
            },
            getCurrentStage: function() {
                if (vm.currentStage) {
                    return vm.currentStage;
                } else {
                    //TODO aller chercher le bon, en cours, ou le dernier de la liste des notre
                    Stage.query(function(result) {
                        vm.currentStage = result[0];
                        return vm.currentStage;
                    });
                }
            }
        };
    }
})();
