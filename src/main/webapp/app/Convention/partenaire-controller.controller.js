(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('PartenaireAjoutController', PartenaireAjoutController);

    PartenaireAjoutController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Partenaire','Activite', 'Region'];

    function PartenaireAjoutController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Partenaire, Activite, Region) {
        var vm = this;

        vm.partenaire = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.activites = Activite.query();
        vm.regions = Region.query();



        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.partenaire.id !== null) {
                Partenaire.update(vm.partenaire, onSaveSuccess, onSaveError);
            } else {
                Partenaire.save(vm.partenaire, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('projectTaaGliApp:partenaireUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datecreation = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
