(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('StageConventionController', StageConventionController);


    StageConventionController.$inject = ['$scope','$stateParams', 'entity', 'Stage','$log','Etudiant','sharedProperties','sharedPropertiesEtudiant'];

    function StageConventionController ($scope,$stateParams, entity, Stage,$log,Etudiant,sharedProperties,sharedPropertiesEtudiant) {

        var vm = this;

       vm.stage = entity;
       vm.etudiants = Etudiant.query();

        vm.etudiant = vm.etudiants[0];
        vm.save = save;
        vm.datePickerOpenStatus = {};
        vm.openCalendar1 = openCalendar1;
        vm.openCalendar2 = openCalendar2;
        vm.openCalendar3 = openCalendar3;
        vm.rs={};

        function save () {
            vm.stage.etudiant = vm.etudiants[0];
            console.log("okeeeey")
          if (vm.stage.id !== null) {
          vm.stage.etudiant=sharedPropertiesEtudiant.getPropertyEtudiant();
            Stage.update(vm.stage,function(result) {
                vm.rs = result;
            sharedProperties.setProperty(vm.rs);
                console.log(vm.rs);
                console.log(vm.rs.id);
            });

          } else {
           vm.stage.etudiant=sharedPropertiesEtudiant.getPropertyEtudiant();
            Stage.save(vm.stage,function(result) {
                vm.rs = result;
                  sharedProperties.setProperty(vm.rs);
                  console.log(vm.rs);
                  console.log(vm.rs.id);
            });
}
            }

            $scope.datepickerOptions = {
                        datepickerMode: "year",
                        minMode: "year",
                        minDate: "minDate",
                        showWeeks: "false",
                    };

            vm.datePickerOpenStatus.sannee = false;
            function openCalendar1 (date) {
                vm.datePickerOpenStatus[date] = true;
            }
            vm.datePickerOpenStatus.datedebut = false;
            function openCalendar2 (date) {
                vm.datePickerOpenStatus[date] = true;
            }
            vm.datePickerOpenStatus.datefin = false;
            function openCalendar3 (date) {
                vm.datePickerOpenStatus[date] = true;
            }

        }


})();
