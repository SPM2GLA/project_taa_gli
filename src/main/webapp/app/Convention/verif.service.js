(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('sharedProperties',sharedProperties);



    function sharedProperties () {
      var hello

      return {
          getProperty: function () {
              return  hello;
          },
          setProperty: function(value) {
             hello= value;
          }
      };
    }


})();
