(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ContactNewController', ContactNewController);

  ContactNewController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Contact', 'Partenaire', 'Stage','sharedProperties','$log'];

    function ContactNewController($timeout, $scope, $stateParams, $uibModalInstance, entity, Contact, Partenaire, Stage,sharedProperties,$log) {
        var vm = this;

        vm.contact = entity;
        vm.clear = clear;
        vm.save = save;
        vm.partenaires = Partenaire.query();



        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.contact.id !== null) {
                Contact.update(vm.contact, onSaveSuccess, onSaveError);
               vm.stage=sharedProperties.getProperty();
                console.log(vm.stage);
                vm.stage.contacts=[];
                  vm.stage.contacts[0]=vm.contact;
                  console.log(vm.stage.contacts[0]);
                Stage.update(vm.stage);
                  console.log(vm.stage);
            } else {
                Contact.save(vm.contact, onSaveSuccess, onSaveError);
               vm.stage=sharedProperties.getProperty();
                console.log(vm.stage);
                vm.stage.contacts=[];
                  vm.stage.contacts[0]=vm.contact;
                  console.log(vm.stage.contacts[0]);
                  Stage.update(vm.stage);
                  console.log(vm.stage);

            }

        }

        function onSaveSuccess (result) {
            $scope.$emit('projectTaaGliApp:contactUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
