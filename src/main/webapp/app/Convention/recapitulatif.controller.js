(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('RecapitulatifController', RecapitulatifController);

    RecapitulatifController.$inject = ['$scope', '$state', 'Stage','sharedProperties'];

    function RecapitulatifController ($scope, $state, Stage,sharedProperties) {
        var vm = this;

        vm.stage = {};

        loadStage();

        function loadStage() {
           vm.stage=sharedProperties.getProperty();
           

        }
    }
})();
