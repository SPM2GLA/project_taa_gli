(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('sharedPropertiesEtudiant',sharedPropertiesEtudiant);




    function sharedPropertiesEtudiant () {
      var student

      return {
          getPropertyEtudiant: function () {
              return  student;
          },
          setPropertyEtudiant: function(value) {
            student= value;
          }
      };
    }




})();
