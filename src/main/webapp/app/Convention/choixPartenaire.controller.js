(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('ChoixPartenaireController', ChoixPartenaireController);

    ChoixPartenaireController.$inject = ['$scope','$state','Partenaire'];

    function ChoixPartenaireController ($scope,$state, Partenaire) {
        var vm = this;
        vm.partenaires = [];

        loadAll();


        function loadAll() {
            Partenaire.query(function(result) {
                vm.partenaires = result;
            });
        }



      }

})();
