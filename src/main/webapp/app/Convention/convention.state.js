(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('AccueilConvention', {
            parent: 'app',
            url: '/AccueilConvention',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/accueilConvention.html',
                    controller: 'ConventionController',
                    controllerAs: 'vm'

                }
            },

        })
        .state('CreateNewConvention', {
            parent: 'etudiant',
            url: '/createconvention',
            data: {
                authorities: []
            },

            views: {
                'content@': {
                    templateUrl: 'app/Convention/createnewconvention.html',
                    controller: 'createNewConventionController',
                    controllerAs: 'vm'

                }
            },

        })
        .state('verificationStudent', {
            parent: 'app',
            url: '/verificationStudent',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/verificationStudent.html',
                    controller: 'verificationStudentController',
                    controllerAs: 'vm'

                }
            },

        })
        .state('ChoixPartenaire', {
            parent: 'app',
            url: '/ChoixPartenaire',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/choixPartenaire.html',
                    controller: 'ChoixPartenaireController',
                    controllerAs: 'vm'
         }
            },

        })
        .state('Show-detail-entreprise', {
            parent: 'app',
            url: '/detailsEntreprise/{id}',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/partenaireDetail.html',
                    controller: 'PartenaireDetailController',
                    controllerAs: 'vm',
                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('partenaire');
                            return $translate.refresh();
                        }],
                        entity: ['$stateParams', 'Partenaire', function($stateParams, Partenaire) {
                            return Partenaire.get({id : $stateParams.id}).$promise;
                        }],
                        previousState: ["$state", function ($state) {
                            var currentStateData = {
                                name: $state.current.name || 'partenaire',
                                params: $state.params,
                                url: $state.href($state.current.name, $state.params)
                            };
                            return currentStateData;
                        }]
                    }
                }
            },

        })
        .state('partenaire-newcreate', {
            parent: 'entity',
            url: '/CreateNewPartenaire',
            data: {
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/Convention/createNewPartenaire.html',
                    controller: 'PartenaireAjoutController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nom: null,
                                adresse: null,
                                datecreation: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('ChoixPartenaire', null, { reload: 'ChoixPartenaire' });
                }, function() {
                    $state.go('ChoixPartenaire');
                });
            }]

        })

        .state('CreationStage', {
            parent: 'stage',
            url: '/CreationStage',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/creationStage.html',
                    controller: 'StageConventionController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                      entity: ['Stage',function(Stage) {

                      }]
                    }
                }
            },

        })
        .state('AffichageContacts', {
            parent: 'app',
            url: '/AffichageContacts',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/affichageContacts.html',
                    controller: 'affichageContactsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contact');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }

        })
        .state('createNewContact', {
            parent: 'entity',
            url: '/newContact',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/Convention/createNewContact.html',
                    controller: 'ContactNewController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nom: null,
                                ntel: null,
                                email: null,
                                sex: null,
                                id: null

                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('AffichageContacts', null, { reload: 'AffichageContacts' });
                }, function() {
                    $state.go('AffichageContacts');
                });
            }]
        })
        .state('ContactDetail', {
            parent: 'entity',
            url: '/contactDetail/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'projectTaaGliApp.contact.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/contactDetail.html',
                    controller: 'ContactDetController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contact');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Contact', function($stateParams, Contact) {
                    return Contact.get({id : $stateParams.id}).$promise;
                }]

            }
        })
        .state('choixEnseignant', {
            parent: 'stage',
            url: '/choixEnseignant',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/choixEnseignant.html',
                    controller: 'choixEnseignantController',
                    controllerAs: 'vm'


                }
            },

        })
        .state('Recapitulatif', {
            parent: 'stage',
            url: '/recapitulatif',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/recapitulatif.html',
                    controller: 'RecapitulatifController',
                    controllerAs: 'vm'


                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('stage');
                    return $translate.refresh();
                }]
              }

        })
        .state('FinConvention', {
            parent: 'stage',
            url: '/Fin',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/Convention/finconvention.html',

                }
            }

        });





    }
})();
