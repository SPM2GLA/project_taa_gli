(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('sharedPropertiesPartenaire',sharedPropertiesPartenaire);




    function sharedPropertiesPartenaire () {
      var partner

      return {
          getPropertyPartenaire: function () {
              return  partner;
          },
          setPropertyPartenaire: function(value) {
             partner= value;
          }
      };
    }




})();
