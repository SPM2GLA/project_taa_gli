(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('EtudiantRegisterDialogController', EtudiantRegisterDialogController);

    EtudiantRegisterDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Etudiant', 'Stage', 'Enquete', 'User'];

    function EtudiantRegisterDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Etudiant, Stage, Enquete, User) {
        var vm = this;

        vm.etudiant = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.fonctions = Etudiant.query({filter: 'etudiant-is-null'});
        $q.all([vm.etudiant.$promise, vm.fonctions.$promise]).then(function() {
            if (!vm.etudiant.fonction || !vm.etudiant.fonction.id) {
                return $q.reject();
            }
            return Etudiant.get({id : vm.etudiant.fonction.id}).$promise;
        }).then(function(fonction) {
            vm.fonctions.push(fonction);
        });
        vm.stages = Stage.query();
        vm.enquetes = Enquete.query();
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.etudiant.id !== null) {
                Etudiant.update(vm.etudiant, onSaveSuccess, onSaveError);
            } else {
                Etudiant.save(vm.etudiant, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('projectTaaGliApp:etudiantUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datenaissance = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
