(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('etudiantregister', {
            parent: 'app',
            url: '/etudiantregister',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'projectTaaGliApp.etudiantRegister.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/etudiantregister/etudiantregister.html',
                    controller: 'etudiantregisterController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('etudiant');
                    $translatePartialLoader.addPart('etudiantRegister');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('etudiantregister.new', {
            parent: 'etudiantregister',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/etudiantregister/etudiantregister-dialog.html',
                    controller: 'EtudiantRegisterDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                            	nom: null,
                                prenom: null,
                                age: null,
                                adresse: null,
                                ntel: null,
                                datenaissance: null,
                                email: null,
                                assurance: null,
                                affiliation: null,
                                sesan: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('etudiantregister', null, { reload: 'etudiantregister' });
                }, function() {
                    $state.go('etudiantregister');
                });
            }]
        });
    }

})();
