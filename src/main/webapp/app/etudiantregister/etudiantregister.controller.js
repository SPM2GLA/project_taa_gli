(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('etudiantregisterController', EtudiantRegisterController);

    EtudiantRegisterController.$inject = ['$scope', 'Principal', '$state', 'Etudiant'];

    function EtudiantRegisterController ($scope, Principal, $state, Etudiant) {
        var vm = this;
        
        vm.etudiants = [];
        loadAll();

        function loadAll() {
        	console.log("Bonjour");
            
        	Etudiant.query(function(result) {
                vm.etudiants = result;
            });
        }
    }
})();