(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('EnquetesNewController', EnquetesNewController);

    EnquetesNewController.$inject = ['$scope', 'Principal', '$state', 'Etudiant', 'Enquete', '$q', 'TexteEnquete', 'ModeObtention', 'Stage', 'Enquetes', 'entity', 'entity2'];

    function EnquetesNewController($scope, Principal, $state, Etudiant, Enquete, $q, TexteEnquete, ModeObtention, Stage, Enquetes, entity, entity2) {
        var vm = this;
        vm.etudiants = [];
        vm.enquetes = [];
        vm.stages = Stage.query();
        vm.modeObtentions = ModeObtention.query();
        vm.texteEnquetes = [];
        vm.openCalendar = openCalendar;
        vm.datePickerOpenStatus = {};
        vm.doTheMagic = doTheMagic;
        vm.questionR = 255;

        vm.texteenquete = entity2;
        vm.enquete = entity;
        vm.filtreannee = new Date("2016-01-01T00:00:00.000Z");
        vm.filtremodeObtention = null;
        vm.nbetudiantannee = 0;
        vm.etudiantannee = [];
        vm.stageannee = [];
        vm.question = "";
        loadAll();



        function loadAll() {

            Etudiant.query(function(result) {
                vm.etudiants = result;
            });
        }


        $scope.datepickerOptions = {
            datepickerMode: "year",
            minMode: "year",
            minDate: "minDate",
            showWeeks: "false",
        };

        vm.datePickerOpenStatus.annee = false;

        function openCalendar(date) {
            console.log("clickcalendar");
            vm.datePickerOpenStatus[date] = true;
        }

        $scope.$watch('vm.filtreannee', function(newvalue, oldvalue) {
            console.log("Maj filtreannee " + vm.filtreannee.getUTCFullYear());
            Enquetes.getstageYear(vm.filtreannee.getUTCFullYear()).then(function(result) {
                // console.log("call callback");
                vm.stageannee = result.data;
                // console.log(vm.stageannee);
                vm.nbetudiantannee = vm.stageannee.length;
                vm.etudiantannee = [];
                for (var i = 0; i < vm.stageannee.length; i++) {
                    vm.etudiantannee.push(vm.stageannee[i].etudiant);
                }
                // console.log(vm.etudiantannee);
            });
        });

        function doTheMagic() {
            console.log("doTheMagic");
            //crée une enquete avec vm.etudiantannee, vm.filtremodeObtention
            //crée vm.nbetudiantannee texteEnquetes avec vm.question, vm.etudiantannee[i], enquete.id

            vm.enquete.id = null;
            vm.enquete.etudiants = null //vm.etudiantannee;
            vm.enquete.modeobtention = vm.filtremodeObtention;
            console.log("createEnq");
            console.log(vm.enquete);
            saveEnq1();
            // crée une enquete sans etudiant parce que sinon erreur objet detacher, enquete a pas d'id mais etudiant si ¯\(°_o)/¯
        }

        function saveEnq1() {
            vm.isSaving = true;
            // console.log("save");
            Enquete.save(vm.enquete, onSaveSuccessEnq1, onSaveErrorEnq);
        }

        function onSaveSuccessEnq1(result) {
            // console.log("onSaveSuccess enq1");
            vm.enquete = result;
            // console.log(result);
            vm.enquete.etudiants = vm.etudiantannee;
            $scope.$emit('projectTaaGliApp:enqueteUpdate', result);
            vm.isSaving = false;
            saveEnq2();
        }

        function saveEnq2() {
            vm.isSaving = true;
            // console.log("update");
            Enquete.update(vm.enquete, onSaveSuccessEnq2, onSaveErrorEnq);
        }

        var i = 0;
        function onSaveSuccessEnq2(result) {
            // console.log("onSaveSuccess enq2");
            // console.log(result);
            vm.enquete = result;
            $scope.$emit('projectTaaGliApp:enqueteUpdate', result);
            vm.isSaving = false;

            i = 0;

                vm.texteenquete.dureerecherche = null;
                vm.texteenquete.enquete = vm.enquete;
                vm.texteenquete.etudiant = vm.etudiantannee[i]; //null;
                vm.texteenquete.id = null;
                vm.texteenquete.question = vm.question;
                vm.texteenquete.reponse = null;
                vm.texteenquete.salaire = null;
                // console.log("createTxt "+vm.texteenquete.etudiant.nom);
                // console.log(vm.texteenquete);
                i++;
                saveTxt();

        }

        function onSaveErrorEnq() {
            console.log("onSaveError enq");
            vm.isSaving = false;
        }
        //original Object { reponse: null, question: "beau temps ?", dureerecherche: null, salaire: null, id: null, etudiant: Object, enquete: Object }
        //lamienne Object { reponse: null, question: "1", dureerecherche: null, salaire: null, id: null, enquete: Object, etudiant: null }
        function saveTxt() {
            vm.isSaving = true;
            if (vm.texteenquete.id !== null) {
                // console.log("update");
                TexteEnquete.update(vm.texteenquete, onSaveSuccessTxt, onSaveErrorTxt);
            } else {
                // console.log("save");
                TexteEnquete.save(vm.texteenquete, onSaveSuccessTxt, onSaveErrorTxt);
            }
        }


        function onSaveSuccessTxt(result) {
            console.log("onSaveSuccess txt");
            $scope.$emit('projectTaaGliApp:enqueteUpdate', result);
            vm.isSaving = false;
            // console.log("i ="+i+" < nb="+vm.nbetudiantannee);
             if(i < vm.nbetudiantannee){
               console.log("i ="+i+" < nb="+vm.nbetudiantannee+ "   doing +1");

               vm.texteenquete.etudiant = vm.etudiantannee[i];
               i++;
              //  console.log("createTxt "+vm.texteenquete.etudiant.nom);
              //  console.log(vm.texteenquete);
               saveTxt();
             }
             else {
               console.log("i ="+i+" < nb="+vm.nbetudiantannee+ "   doing else");
               $state.go("enquetes");
             }
        }

        function onSaveErrorTxt() {
            console.log("onSaveError txt");
            vm.isSaving = false;
        }

        //affichage de la taille max 255 characteres de question
        $scope.$watch('vm.question', function(newvalue, oldvalue) {
            vm.questionR = 255 - vm.question.length;

        });
    }
})();
