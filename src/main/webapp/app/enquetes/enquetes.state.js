(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('enquetes', {
            parent: 'app',
            url: '/enquetes',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'projectTaaGliApp.enquetes.home.title1'
            },
            views: {
                'content@': {
                    templateUrl: 'app/enquetes/enquetes.html',
                    controller: 'EnquetesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('etudiant');
                    $translatePartialLoader.addPart('enquete');
                    $translatePartialLoader.addPart('texteEnquete');
                    $translatePartialLoader.addPart('modeObtention');
                    $translatePartialLoader.addPart('enquetes');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('enquetes-mode', {
            parent: 'enquetes-new',
            url: '/newmode',
            ata: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/enquetes/enquetes-mode-dialog.html',
                    controller: 'EnquetesModeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('enquetes-new', null, { reload: 'enquetes-new' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('enquetes-new', {
            parent: 'enquetes',
            url: '/newenquetes',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'projectTaaGliApp.enquetes.home.title2'
            },
            views: {
                'content@': {
                    templateUrl: 'app/enquetes/enquetes-new.html',
                    controller: 'EnquetesNewController',
                    controllerAs: 'vm',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        },
                        entity2: function () {
                            return {
                                reponse: null,
                                question: null,
                                dureerecherche: null,
                                salaire: null,
                                id: null
                            };
                        }
                    }
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('etudiant');
                    $translatePartialLoader.addPart('enquete');
                    $translatePartialLoader.addPart('texteEnquete');
                    $translatePartialLoader.addPart('modeObtention');
                    $translatePartialLoader.addPart('enquetes');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('enquetes-reponse', {
            parent: 'enquetes',
            url: '/{id}/reponse',
            ata: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/enquetes/enquetes-reponse.html',
                    controller: 'EnquetesReponseController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TexteEnquete', function(TexteEnquete) {
                            return TexteEnquete.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('enquetes', null, { reload: 'enquetes' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
