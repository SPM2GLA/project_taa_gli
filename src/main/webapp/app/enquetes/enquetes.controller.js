(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('EnquetesController', EnquetesController);

    EnquetesController.$inject = ['$scope', 'Principal', '$state', 'Etudiant', 'Enquete', 'TexteEnquete', 'ModeObtention', 'Enquetes'];

    function EnquetesController($scope, Principal, $state, Etudiant, Enquete, TexteEnquete, ModeObtention, Enquetes) {
        var vm = this;
        vm.enqquetes = [];
        vm.modeObtentions = ModeObtention.query();
        vm.texteEnquetes = [];
        vm.loadAll = loadAll;
        vm.filtremodeObtention = Enquetes.getMethodeObtention();
        vm.filtre = Enquetes.getFilter();
        loadAll();

        //utilisation du service pour mémoriser les paramètres après l'édition des réponces
        $scope.$watch('vm.filtremodeObtention', function(newvalue, oldvalue) {
            console.log("Maj filtremodeObtention " + vm.filtremodeObtention);
            Enquetes.setMethodeObtention(vm.filtremodeObtention);
        });

        $scope.$watch('vm.filtre', function(newvalue, oldvalue) {
            console.log("Maj filtre " + vm.filtre);
            Enquetes.setFilter(vm.filtre);
        });

        function loadAll() {
            Enquete.query(function(result) {
                vm.enqquetes = result;
                // console.log("enquete : ");
                // console.log(result);
            });
            TexteEnquete.query(function(result) {
                vm.texteEnquetes = result;
                // console.log("texteenquete : ");
                // console.log(vm.texteEnquetes);
                for (var y = 0; y < vm.enqquetes.length; y++) {
                    vm.enqquetes[y].texteEnquetes = [];
                }
                console.log(vm.enqquetes);
                for (var i = 0; i < vm.texteEnquetes.length; i++) {
                    for (var y = 0; y < vm.enqquetes.length; y++) {
                        if (vm.enqquetes[y].id === vm.texteEnquetes[i].enquete.id) {
                          console.log("assign "+vm.texteEnquetes[i].etudiant.nom+" to enquete "+vm.enqquetes[y].id);
                            vm.enqquetes[y].texteEnquetes.push(vm.texteEnquetes[i]);

                        }
                    }
                }
                // for (var y = 0; y < vm.enqquetes.length; y++) {
                //     vm.texteEnquetes[i].enquete = null;
                // }
                console.log(vm.enqquetes);
            });
        }
    }
})();
