(function() {
    'use strict';
    angular
        .module('projectTaaGliApp')
        .factory('Enquetes', Enquetes);

    Enquetes.$inject = ['$http'];

    function Enquetes($http) {

      var Filter = null;
      var MethodeObtention = null;
        return {
            getstageYear: function(year) {
                return $http.get('api/stagesYear/'+year, {
                }).then(
                    function(response) {
                        return response;
                  });
            },
            setMethodeObtention: function(param) {
                MethodeObtention = param;
            },
            getMethodeObtention: function() {
                return MethodeObtention;
            },
            setFilter: function(param) {
                Filter = param;
            },
            getFilter: function() {
                return Filter;
            }
        };
    }
})();
