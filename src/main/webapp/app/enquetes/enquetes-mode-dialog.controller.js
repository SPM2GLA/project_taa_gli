(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('EnquetesModeDialogController', EnquetesModeDialogController);

    EnquetesModeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ModeObtention', 'Enquete'];

    function EnquetesModeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ModeObtention, Enquete) {
        var vm = this;

        vm.modeObtention = entity;
        vm.clear = clear;
        vm.save = save;
        vm.enquetes = Enquete.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.modeObtention.id !== null) {
                ModeObtention.update(vm.modeObtention, onSaveSuccess, onSaveError);
            } else {
                ModeObtention.save(vm.modeObtention, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('projectTaaGliApp:modeObtentionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
