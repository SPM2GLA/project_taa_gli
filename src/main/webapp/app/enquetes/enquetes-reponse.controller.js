(function() {
    'use strict';

    angular
        .module('projectTaaGliApp')
        .controller('EnquetesReponseController', EnquetesReponseController);

    EnquetesReponseController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TexteEnquete', 'Enquete', 'Etudiant'];

    function EnquetesReponseController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TexteEnquete, Enquete, Etudiant) {
              var vm = this;

              vm.texteEnquete = entity;
              vm.clear = clear;
              vm.save = save;
              vm.enquetes = Enquete.query();
              vm.etudiants = Etudiant.query();
              vm.questionR = 255;
              vm.reponseR = 255;

              $timeout(function (){
                  angular.element('.form-group:eq(1)>input').focus();
              });

              function clear () {
                  $uibModalInstance.dismiss('cancel');
              }

              function save () {
                console.log(vm.texteEnquete);
                  vm.isSaving = true;
                  if (vm.texteEnquete.id !== null) {
                      TexteEnquete.update(vm.texteEnquete, onSaveSuccess, onSaveError);
                  } else {
                      TexteEnquete.save(vm.texteEnquete, onSaveSuccess, onSaveError);
                  }
              }

              function onSaveSuccess (result) {
                  $scope.$emit('projectTaaGliApp:texteEnqueteUpdate', result);
                  $uibModalInstance.close(result);
                  vm.isSaving = false;
              }

              function onSaveError () {
                  vm.isSaving = false;
              }

              //affichage de la taille max 255 characteres de question
              $scope.$watch('vm.texteEnquete.question', function(newvalue, oldvalue) {
                  vm.questionR = 255 - vm.texteEnquete.question.length;

              });

              //affichage de la taille max 255 characteres de reponse
              $scope.$watch('vm.texteEnquete.reponse', function(newvalue, oldvalue) {
                  vm.reponseR = 255 - vm.texteEnquete.reponse.length;

              });
          }
      })();
