package fr.istic.elgapiton.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import fr.istic.elgapiton.domain.Contact;
import fr.istic.elgapiton.domain.Etudiant;
import fr.istic.elgapiton.domain.Stage;
import fr.istic.elgapiton.repository.ContactRepository;
import fr.istic.elgapiton.repository.EtudiantRepository;
import fr.istic.elgapiton.repository.StageRepository;
import fr.istic.elgapiton.security.AuthoritiesConstants;
import fr.istic.elgapiton.security.SecurityUtils;
import fr.istic.elgapiton.web.rest.util.HeaderUtil;

import org.jboss.logging.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Stage.
 */
@RestController
@RequestMapping("/api")
public class StageResource {

    private final Logger log = LoggerFactory.getLogger(StageResource.class);
        
    @Inject
    private StageRepository stageRepository;
    
    
    /**
     * POST  /stages : Create a new stage.
     *
     * @param stage the stage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new stage, or with status 400 (Bad Request) if the stage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/stages",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Stage> createStage(@Valid @RequestBody Stage stage) throws URISyntaxException {
        log.debug("REST request to save Stage : {}", stage);
        System.err.println("Okeeeeeeeeeeeeeeeeeey Staaaaaaaaaaaaaage"); 
        if (stage.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("stage", "idexists", "A new stage cannot already have an ID")).body(null);
        }
        Stage result = stageRepository.save(stage);
        return ResponseEntity.created(new URI("/api/stages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("stage", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /stages : Updates an existing stage.
     *
     * @param stage the stage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated stage,
     * or with status 400 (Bad Request) if the stage is not valid,
     * or with status 500 (Internal Server Error) if the stage couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/stages",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Stage> updateStage(@Valid @RequestBody Stage stage) throws URISyntaxException {
        log.debug("REST request to update Stage : {}", stage);
        System.err.println("nooooooooooooooooooooooooooooooooo");
        System.err.println(stage.getContacts());
        if (stage.getId() == null) {
            return createStage(stage);
        }
        Stage result = stageRepository.save(stage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("stage", stage.getId().toString()))
            .body(result);
    }

   

    /**
     * GET  /stages : get all the stages.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of stages in body
     */
    @RequestMapping(value = "/stages",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Stage> getAllStages() {
        log.debug("REST request to get all Stages");
        List<Stage> stages = null;//= stageRepository.findAllWithEagerRelationships();
        
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) { 
        	stages = stageRepository.findAll(); 
          }  
          else { 
            stages = stageRepository.findAllForCurrentUser(); 
          } 
        return stages;
    }

    /**
     * GET  /stages/:id : get the "id" stage.
     *
     * @param id the id of the stage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the stage, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/stages/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Stage> getStage(@PathVariable Long id) {
        log.debug("REST request to get Stage : {}", id);
        Stage stage = stageRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(stage)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
   /* @RequestMapping(value = "/stages/{partner}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        public List<Stage>  getStageOfPartner(@PathVariable(value="partner") String partner) {
            log.debug("REST request to get Stage");
            System.err.println("Okeeeeeeeeeeeeeeeeeey Staaaaaaaaaaaaaage Partenaiiiire");
            List<Stage> stages = stageRepository.findAllWithPartner(partner);
            return stages;
        }*/

    /**
     * DELETE  /stages/:id : delete the "id" stage.
     *
     * @param id the id of the stage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/stages/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteStage(@PathVariable Long id) {
        log.debug("REST request to delete Stage : {}", id);
        stageRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("stage", id.toString())).build();
    }

    
    
    /**
     * GET  /stage/pdf/:id : get the pdf for "id" stage.
     *
     * @param id the id of the stage to retrieve
     * @return 
     * @return tdb
     */
    @RequestMapping(value = "/stage/pdf/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Timed
    public   ResponseEntity<Object> getpdfStage(@PathVariable Long id) throws Exception {
    	System.err.println("in getpdfStage Rest PDF" +id);
    	log.debug("REST request to get stage : {}", id);

    	Stage stage = stageRepository.findOneWithEagerRelationships(id);

    	try {
    		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    		ByteArrayOutputStream file = new ByteArrayOutputStream(); 
    		Document document = new Document();
    		PdfWriter.getInstance(document, file);
    		document.open();	//PDF document opened........		

    		try {
	    		//Inserting Rennes1 logo in PDF
	    		Image image = Image.getInstance ("src/main/resources/RENNES1.PNG");
	    		image.scaleAbsolute(171f, 76f);	//image width,height	
	    		document.add(image);
    		} catch (Exception e) {
    			System.out.println("corrupted png, again");
    		}

    		//Inserting title convention n° in PDF
    		PdfPTable titletable=new PdfPTable(12);
    		PdfPCell titletitle = new PdfPCell (new Paragraph ("CONVENTION DE STAGE n°"+stage.getId().toString()+"\n"+"Stagiaire de formation continue"));
    		//block pour le titre
    		titletitle.setColspan (12);
    		titletitle.setHorizontalAlignment (Element.ALIGN_CENTER);
    		titletitle.setPadding (10.0f);
    		//    	titletitle.setBackgroundColor (new BaseColor (140, 221, 8));	
    		titletable.addCell(titletitle);
    		document.add(titletable);

    		//txt
    		System.err.println(">>");
    		Set<Contact> scontacts = stage.getPartenaire().getContacts();
    		System.err.println(">>>");
    		Iterator<Contact> contacts = scontacts.iterator();
    		System.err.println(">>>>");
    		Contact contact = contacts.next();
    		System.err.println(">>>>>");
    		document.add(new Paragraph("La présente convention règle les rapports entre les soussignés :"));
    		document.add(new Paragraph("Adresse : "+stage.getPartenaire().getAdresse()));
    		document.add(new Paragraph("Tél : "+contact.getNtel()+"  mél : "+contact.getEmail()));
    		document.add(new Paragraph("Représenté par : (nom du signataire de la convention) : "+contact.getNom()));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Et"));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("L'Université de Rennes 1"));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Et"));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Le stagiaire :"));
    		document.add(new Paragraph("Nom : "+stage.getEtudiant().getNom()+" Prénom : "+stage.getEtudiant().getPrenom()));
    		document.add(new Paragraph("Né(e) le : "+stage.getEtudiant().getDatenaissance().getDayOfMonth()+"/"+stage.getEtudiant().getDatenaissance().getMonthValue()+"/"+stage.getEtudiant().getDatenaissance().getYear()));
    		document.add(new Paragraph("Adresse : "+stage.getEtudiant().getAdresse()));
    		document.add(new Paragraph("Tél : "+stage.getEtudiant().getNtel()+" Mél : "+stage.getEtudiant().getEmail()));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Stagiaire de formation continue inscrit(e) à l'Université de Rennes 1 en : " + stage.getDiplomeifsic().getLibelle()));
    		if(stage.getDiplomeifsic().getFiliere() != null)
    		document.add(new Paragraph("Composante de rattachement : "+stage.getDiplomeifsic().getFiliere().getLibelle()));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Il a été convenu ce qui suit :"));
    		document.add(new Paragraph("Article 1 : Objet, contenu et encadrement du stage"));
    		document.add(new Paragraph("Le présent stage a une finalité pédagogique. Il s'inscrit dans le cadre de la formation et du projet personnel et professionnel du stagiaire. Il lui permet d'utiliser les acquis de sa formation, de la compléter et faciliter son intégration dans le monde professionnel."));
    		document.add(new Paragraph("L'encadrement du stage est assuré :"));
    		document.add(new Paragraph("pour la structure d'accueil, par :"));
    		if(contacts.hasNext())
    			contact = contacts.next();
    		document.add(new Paragraph("Nom : "+contact.getNom()));
    		document.add(new Paragraph("Tél : "+contact.getNtel()));
    		document.add(new Paragraph("Mél : "+contact.getEmail()));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("pour l'Université de Rennes 1, par "));
    		document.add(new Paragraph("Nom : "+stage.getEnseignant().getNom()+" "+stage.getEnseignant().getPrenom()));
    		document.add(new Paragraph("Tél : "+stage.getEnseignant().getNtel()));
    		document.add(new Paragraph("Mél : "+stage.getEnseignant().getNom()+"."+stage.getEnseignant().getPrenom()+"@univ-rennes1.fr"));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Le stage entre dans le cursus pédagogique du stagiaire : il a pour objet de lui permettre de mettre en pratique les outils théoriques et méthodologiques acquis au cours de sa formation, d'identifier ses compétences et de conforter son projet professionnel."));
    		document.add(new Paragraph("Le sujet du stage, établi par la structure d'accueil, en accord avec le stagiaire et le tuteur du stage à l'Université, est le suivant : "+stage.getSujet()));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("blabla..."));
    		document.add(new Paragraph("Article 2 : Durée et dates du stage - interruption et absences"));
    		document.add(new Paragraph("Date du stage : du "+stage.getPeriodestage().getDatedebut()+" au "+stage.getPeriodestage().getDatefin()));
    		document.add(new Paragraph("blabla..."));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Article 3 : Déroulement du stage"));
    		document.add(new Paragraph("blabla..."));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Article 4 : Gratification et avantages en nature"));
    		if(stage.getSalaire() != null)
    		document.add(new Paragraph("Gratification prévue : "+stage.getSalaire()+" euros Brut par mois."));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Article 5,6,7,8,9,10"));
    		document.add(new Paragraph("blabla..."));
    		document.add(new Paragraph(" "));
    		document.add(new Paragraph("Fait à                                    , le :"));
    		document.add(new Paragraph(" "));
    		
    		//Inserting Table in PDF
    		PdfPTable table=new PdfPTable(3);
    		PdfPCell title = new PdfPCell (new Paragraph ("Signatures"));

    		//block pour le titre
    		title.setColspan (3);
    		title.setHorizontalAlignment (Element.ALIGN_CENTER);
    		title.setPadding (10.0f);
    		title.setBackgroundColor (new BaseColor (140, 221, 8));					               
    		table.addCell(title);

    		table.setSpacingBefore(90.0f);       // Space Before table starts, like margin-top in CSS
    		table.setSpacingAfter(90.0f);        // Space After table starts, like margin-Bottom in CSS		
    		
    		table.addCell("Lu et approuvé. Pour la structure d'accueil , "+contact.getNom());
    		table.addCell("Lu et approuvé. Pour le stagiaire , "+stage.getEtudiant().getNom()+" "+stage.getEtudiant().getPrenom()+"\n\n\n\n\n\n\n\n\n\n\n");
    		table.addCell("Lu et approuvé. Pour le responsable pédagogique du stagiaire à l'Université de Rennes 1, "+stage.getEnseignant().getNom()+" "+stage.getEnseignant().getPrenom());
    		table.setWidthPercentage(100.0f);
//    		//ajout des titre des collones
//    		table.addCell("Id");
//    		table.addCell("Nom");
//    		table.addCell("Prenom");
//    		table.addCell("Age");
//    		table.addCell("Adresse");
//    		table.addCell("Ntel");
//    		table.addCell("Datenaissance");
//    		table.addCell("Email");
//    		table.addCell("Assurance");
//    		table.addCell("Affiliation");
//    		table.addCell("Fonction");
//    		table.addCell("User");
//
//    		//ajouts de tout le sétudiants
//    		System.err.println("in etudiantRepository");
//    		System.err.println("count : "+etudiantRepository.count());
//    		List<Etudiant> etudiants; 
//    		etudiants = etudiantRepository.findAll(); 
//
//    		for (int i = 0; i < etudiantRepository.count(); i++) {
//    			Etudiant etudiant = etudiants.get(i);
//    			table.addCell(etudiant.getId().toString());
//    			table.addCell(etudiant.getNom().toString());
//    			table.addCell(etudiant.getPrenom().toString());
//    			table.addCell(etudiant.getAge().toString());
//    			table.addCell(etudiant.getAdresse().toString());
//    			table.addCell(etudiant.getNtel().toString());
//    			table.addCell(etudiant.getDatenaissance().toString());
//    			table.addCell(etudiant.getEmail().toString());
//    			table.addCell(etudiant.getAssurance().toString());
//    			table.addCell(etudiant.getAffiliation().toString());
//    			table.addCell(etudiant.getSesan().toString());
//    			table.addCell(etudiant.getUser().getId().toString());
//    		}
    		table.setSpacingBefore(30.0f);       // Space Before table starts, like margin-top in CSS
    		table.setSpacingAfter(30.0f);        // Space After table starts, like margin-Bottom in CSS								          
//
//    		//Inserting List in PDF
//    		com.itextpdf.text.List list=new com.itextpdf.text.List(true,30);
//    		list.add(new ListItem("Item1"));
//    		list.add(new ListItem("Item2"));
//    		list.add(new ListItem("Some Thing..."));		
//
//    		//Text formating in PDF
//    		Chunk chunk=new Chunk("Hello ");
//    		chunk.setUnderline(+1f,-2f);//1st co-ordinate is for line width,2nd is space between
//    		Chunk chunk1=new Chunk("Yousra");
//    		chunk1.setUnderline(+4f,-8f);
//    		chunk1.setBackground(new BaseColor (17, 46, 193));      



//
//    		document.add(Chunk.NEWLINE);   //Something like in HTML :-)
//
//    		document.add(new Paragraph("Informations Etudiants"));
//    		document.add(new Paragraph("Document Generated On - "+new Date().toString()));	

    		document.add(table);
//
//    		document.add(chunk);
//    		document.add(chunk1);
//
//    		document.add(Chunk.NEWLINE);   //Something like in HTML :-)							    
//
//    		document.newPage();            //Opened new page
//    		document.add(list);            //In the new page we are going to add list
    		document.addTitle("convention");
    		document.close();

    		file.close();

    		System.out.println("Pdf created successfully..");


    		InputStreamResource inputStreamResource = new InputStreamResource(new ByteArrayInputStream(file.toByteArray()));
    		return ResponseEntity
    				.ok()
    				.contentLength(file.size())
    				.contentType(MediaType.APPLICATION_PDF)
    				.body(inputStreamResource);
    	} catch (Exception e) {
    		System.err.println("something went wrong while generating the pdf !");
    		e.printStackTrace();
    		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Error");
    	}
    	//return null;
    }

    /**
     * GET  /stage/pdf/:id : get the cvs for "id" stage.
     *
     * @param id the id list of the stages to retrieve
     * @return
     * @return tbd
     */
    @RequestMapping(value = "/stage/csv/{ids}",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public   ResponseEntity<Object> getcvsStage(@PathVariable List<Long> ids) throws Exception {
    	System.err.println("in getpdfStage Rest CVS" +ids);
    	log.debug("REST request to get stage : {}", ids);



    	Stage stage ;
    	try {
    		String txt = "";
    		txt += "Id,Sujet,Salaire,Année,Diplomeifsic,Periodestage,Contact,Etudiant,Partenaire,Enseignant,Fonction\r\n";
    		for (int i = 0; i < ids.size(); i++) {
    			stage = stageRepository.findOneWithEagerRelationships(ids.get(i));
    			System.err.println("stage"+stage);
    			txt += stage.toStringArray();
			}
    		return ResponseEntity
    				.ok()
    				.contentLength(txt.length())
    				.contentType(MediaType.TEXT_PLAIN)
    				.body(txt);

    	} catch (Exception e) {
    		System.err.println("something went wrong while generating the cvs !");
    		e.printStackTrace();
    		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Error");
    	}
//    	return null;
    }
    
    /**
     * GET  /stages : get all the stages for year.
     *
     * @param year the year of the stage to retrieve
     * @return the ResponseEntity with status 200 (OK) and the list of stages in body
     */
    @RequestMapping(value = "/stagesYear/{year}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Stage> getAllStagesYear(@PathVariable int year) {
        log.debug("REST request to get all Stages year: {}", year);
        System.err.println("REST Stages year "+year);
        LocalDate ld= Year.of(year).atDay(1);
        List<Stage> stages = stageRepository.findAllForYear(ld);
        System.err.println("REST Stages year "+year+" nb "+stages.size());
        return stages;
    }

}
