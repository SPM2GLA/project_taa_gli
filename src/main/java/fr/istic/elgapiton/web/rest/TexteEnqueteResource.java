package fr.istic.elgapiton.web.rest;

import com.codahale.metrics.annotation.Timed;

import fr.istic.elgapiton.domain.Stage;
import fr.istic.elgapiton.domain.TexteEnquete;

import fr.istic.elgapiton.repository.TexteEnqueteRepository;
import fr.istic.elgapiton.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TexteEnquete.
 */
@RestController
@RequestMapping("/api")
public class TexteEnqueteResource {

    private final Logger log = LoggerFactory.getLogger(TexteEnqueteResource.class);
        
    @Inject
    private TexteEnqueteRepository texteEnqueteRepository;

    /**
     * POST  /texte-enquetes : Create a new texteEnquete.
     *
     * @param texteEnquete the texteEnquete to create
     * @return the ResponseEntity with status 201 (Created) and with body the new texteEnquete, or with status 400 (Bad Request) if the texteEnquete has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/texte-enquetes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TexteEnquete> createTexteEnquete(@RequestBody TexteEnquete texteEnquete) throws URISyntaxException {
        log.debug("REST request to save TexteEnquete : {}", texteEnquete);
        if (texteEnquete.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("texteEnquete", "idexists", "A new texteEnquete cannot already have an ID")).body(null);
        }
        TexteEnquete result = texteEnqueteRepository.save(texteEnquete);
        return ResponseEntity.created(new URI("/api/texte-enquetes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("texteEnquete", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /texte-enquetes : Updates an existing texteEnquete.
     *
     * @param texteEnquete the texteEnquete to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated texteEnquete,
     * or with status 400 (Bad Request) if the texteEnquete is not valid,
     * or with status 500 (Internal Server Error) if the texteEnquete couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/texte-enquetes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TexteEnquete> updateTexteEnquete(@RequestBody TexteEnquete texteEnquete) throws URISyntaxException {
        log.debug("REST request to update TexteEnquete : {}", texteEnquete);
        if (texteEnquete.getId() == null) {
            return createTexteEnquete(texteEnquete);
        }
        TexteEnquete result = texteEnqueteRepository.save(texteEnquete);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("texteEnquete", texteEnquete.getId().toString()))
            .body(result);
    }

    /**
     * GET  /texte-enquetes : get all the texteEnquetes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of texteEnquetes in body
     */
    @RequestMapping(value = "/texte-enquetes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TexteEnquete> getAllTexteEnquetes() {
        log.debug("REST request to get all TexteEnquetes");
        List<TexteEnquete> texteEnquetes = texteEnqueteRepository.findAll();
        return texteEnquetes;
    }

    /**
     * GET  /texte-enquetes/:id : get the "id" texteEnquete.
     *
     * @param id the id of the texteEnquete to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the texteEnquete, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/texte-enquetes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TexteEnquete> getTexteEnquete(@PathVariable Long id) {
        log.debug("REST request to get TexteEnquete : {}", id);
        TexteEnquete texteEnquete = texteEnqueteRepository.findOne(id);
        return Optional.ofNullable(texteEnquete)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /texte-enquetes/:id : delete the "id" texteEnquete.
     *
     * @param id the id of the texteEnquete to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/texte-enquetes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTexteEnquete(@PathVariable Long id) {
        log.debug("REST request to delete TexteEnquete : {}", id);
        texteEnqueteRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("texteEnquete", id.toString())).build();
    }

    /**
     * GET  /texte-enquetes/csv/:id : get the cvs for "id" stage.
     *
     * @param id the id list of the stages to retrieve
     * @return
     * @return tbd
     */
    @RequestMapping(value = "/texte-enquetes/csv/{ids}",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public   ResponseEntity<Object> getcvsTexteEnquete(@PathVariable List<Long> ids) throws Exception {
    	System.err.println("in getpdfStage Rest CVS" +ids);
    	log.debug("REST request to get stage : {}", ids);



    	TexteEnquete texteEnquete ;
    	try {
    		String txt = "";
    		txt += "Id,Reponse,Question,Dureerecherche,Salaire,Etudiant,Enquete\r\n";
    		for (int i = 0; i < ids.size(); i++) {
    			texteEnquete = texteEnqueteRepository.findOne(ids.get(i));
    			System.err.println("stage"+texteEnquete);
    			txt += texteEnquete.toStringArray();
			}
    		return ResponseEntity
    				.ok()
    				.contentLength(txt.length())
    				.contentType(MediaType.TEXT_PLAIN)
    				.body(txt);

    	} catch (Exception e) {
    		System.err.println("something went wrong while generating the cvs !");
    		e.printStackTrace();
    		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Error");
    	}
//    	return null;
    }
}
