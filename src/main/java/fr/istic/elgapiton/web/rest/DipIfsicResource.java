package fr.istic.elgapiton.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.istic.elgapiton.domain.DipIfsic;
import fr.istic.elgapiton.domain.Stage;
import fr.istic.elgapiton.repository.DipIfsicRepository;
import fr.istic.elgapiton.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing DipIfsic.
 */
@RestController
@RequestMapping("/api")
public class DipIfsicResource {

    private final Logger log = LoggerFactory.getLogger(DipIfsicResource.class);
        
    @Inject
    private DipIfsicRepository dipIfsicRepository;

    /**
     * POST  /dip-ifsics : Create a new dipIfsic.
     *
     * @param dipIfsic the dipIfsic to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dipIfsic, or with status 400 (Bad Request) if the dipIfsic has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dip-ifsics",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DipIfsic> createDipIfsic(@RequestBody DipIfsic dipIfsic) throws URISyntaxException {
        log.debug("REST request to save DipIfsic : {}", dipIfsic);
        if (dipIfsic.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("dipIfsic", "idexists", "A new dipIfsic cannot already have an ID")).body(null);
        }
        DipIfsic result = dipIfsicRepository.save(dipIfsic);
        return ResponseEntity.created(new URI("/api/dip-ifsics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("dipIfsic", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dip-ifsics : Updates an existing dipIfsic.
     *
     * @param dipIfsic the dipIfsic to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dipIfsic,
     * or with status 400 (Bad Request) if the dipIfsic is not valid,
     * or with status 500 (Internal Server Error) if the dipIfsic couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/dip-ifsics",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DipIfsic> updateDipIfsic(@RequestBody DipIfsic dipIfsic) throws URISyntaxException {
        log.debug("REST request to update DipIfsic : {}", dipIfsic);
        if (dipIfsic.getId() == null) {
            return createDipIfsic(dipIfsic);
        }
        DipIfsic result = dipIfsicRepository.save(dipIfsic);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("dipIfsic", dipIfsic.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dip-ifsics : get all the dipIfsics.
     *
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of dipIfsics in body
     */
    @RequestMapping(value = "/dip-ifsics",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DipIfsic> getAllDipIfsics(@RequestParam(required = false) String filter) {
        if ("stage-is-null".equals(filter)) {
            log.debug("REST request to get all DipIfsics where stage is null");
            return StreamSupport
                .stream(dipIfsicRepository.findAll().spliterator(), false)
                .filter(dipIfsic -> dipIfsic.getStage() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all DipIfsics");
        List<DipIfsic> dipIfsics = dipIfsicRepository.findAllWithEagerRelationships();
        return dipIfsics;
    }

    /**
     * GET  /dip-ifsics/:id : get the "id" dipIfsic.
     *
     * @param id the id of the dipIfsic to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dipIfsic, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/dip-ifsics/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DipIfsic> getDipIfsic(@PathVariable Long id) {
        log.debug("REST request to get DipIfsic : {}", id);
        DipIfsic dipIfsic = dipIfsicRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(dipIfsic)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /dip-ifsics/:id : delete the "id" dipIfsic.
     *
     * @param id the id of the dipIfsic to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/dip-ifsics/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDipIfsic(@PathVariable Long id) {
        log.debug("REST request to delete DipIfsic : {}", id);
        dipIfsicRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("dipIfsic", id.toString())).build();
    }
    
    /**
     * GET  /dip-ifsic/pdf/:id : get the cvs for "id" dip-ifsic.
     *
     * @param id the id list of the dip-ifsics to retrieve
     * @return
     * @return tbd
     */
    @RequestMapping(value = "/dip-ifsic/csv/{ids}",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public   ResponseEntity<Object> getcvsDipifsic(@PathVariable List<Long> ids) throws Exception {
    	System.err.println("in getpdfDip-ifsic Rest CVS" +ids);
    	log.debug("REST request to get dip-ifsic : {}", ids);



    	DipIfsic dipIfsic ;
    	try {
    		String txt = "";
    		txt += "Id,Libelle,Enseignant,Filiere\r\n";
    		for (int i = 0; i < ids.size(); i++) {
    			dipIfsic = dipIfsicRepository.findOneWithEagerRelationships(ids.get(i));
    			System.err.println("stage"+dipIfsic);
    			txt += dipIfsic.toStringArray();
			}
    		return ResponseEntity
    				.ok()
    				.contentLength(txt.length())
    				.contentType(MediaType.TEXT_PLAIN)
    				.body(txt);

    	} catch (Exception e) {
    		System.err.println("something went wrong while generating the cvs !");
    		e.printStackTrace();
    		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Error");
    	}
//    	return null;
    }

}
