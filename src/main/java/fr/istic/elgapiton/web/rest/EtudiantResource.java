package fr.istic.elgapiton.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import fr.istic.elgapiton.domain.Authority; 
import fr.istic.elgapiton.domain.Etudiant;
import fr.istic.elgapiton.domain.Stage;
import fr.istic.elgapiton.domain.User;
import fr.istic.elgapiton.repository.EtudiantRepository;
import fr.istic.elgapiton.repository.UserRepository;
import fr.istic.elgapiton.security.AuthoritiesConstants;
import fr.istic.elgapiton.security.SecurityUtils;
import fr.istic.elgapiton.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Etudiant.
 */
@RestController
@RequestMapping("/api")
public class EtudiantResource {

    private final Logger log = LoggerFactory.getLogger(EtudiantResource.class);
        
    @Inject
    private EtudiantRepository etudiantRepository;

    @Inject 
    private UserRepository userRepository; 
    
    /**
     * POST  /etudiants : Create a new etudiant.
     *
     * @param etudiant the etudiant to create
     * @return the ResponseEntity with status 201 (Created) and with body the new etudiant, or with status 400 (Bad Request) if the etudiant has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/etudiants",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Etudiant> createEtudiant(@RequestBody Etudiant etudiant) throws URISyntaxException {
        log.debug("REST request to save Etudiant : {}", etudiant);
        if (etudiant.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("etudiant", "idexists", "A new etudiant cannot already have an ID")).body(null);
        }
        else if(userRepository.findOneByLogin(etudiant.getNom()+"."+etudiant.getPrenom()).isPresent()){ 
            // récupération d'un user existant 
            etudiant.setUser(userRepository.findOneByLogin(etudiant.getNom()+"."+etudiant.getPrenom()).get()); 
             
          } 
          else{ 
   
            // création d'un user si etudiant non liée  
            User user = new User(); 
            user.setActivated(true); 
            user.setLogin(etudiant.getNom()+"."+etudiant.getPrenom()); 
            user.setEmail(""+etudiant.getNom()+"."+etudiant.getPrenom()+"@etudiant.univ-rennes1.fr"); 
            etudiant.setEmail(""+etudiant.getNom()+"."+etudiant.getPrenom()+"@etudiant.univ-rennes1.fr");
            user.setFirstName(etudiant.getNom()); 
            user.setLastName(etudiant.getPrenom()); 
            user.setLangKey("fr"); 
            Set<Authority> s = new HashSet<Authority>(); 
            Authority a = new Authority(); 
            a.setName(AuthoritiesConstants.USER); 
            s.add(a); 
            user.setAuthorities(s); 
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(); 
            user.setPassword(encoder.encode(etudiant.getNom()+"."+etudiant.getPrenom())); 
            userRepository.save(user); 
            etudiant.setUser(user); 
          } 
        Etudiant result = etudiantRepository.save(etudiant);
        return ResponseEntity.created(new URI("/api/etudiants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("etudiant", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /etudiants : Updates an existing etudiant.
     *
     * @param etudiant the etudiant to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated etudiant,
     * or with status 400 (Bad Request) if the etudiant is not valid,
     * or with status 500 (Internal Server Error) if the etudiant couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/etudiants",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Etudiant> updateEtudiant(@RequestBody Etudiant etudiant) throws URISyntaxException {
        log.debug("REST request to update Etudiant : {}", etudiant);
        System.err.println("okeeeeeeeeeeey");
        if (etudiant.getId() == null) {
            return createEtudiant(etudiant);
        }
        /*else if (userRepository.findOneByLogin(etudiant.getNom()+"."+etudiant.getPrenom()) != null) { 
            //si user déjà présent 
                 
          } */
        
        	//si user non présent cas impossible normalement 
        	System.err.println("erreur update étudiant, pass de user associer"); 
        
      // etudiant.setAffiliation(affiliation);
        Etudiant result = etudiantRepository.save(etudiant);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("etudiant", etudiant.getId().toString()))
            .body(result);
        
    }

    /**
     * GET  /etudiants : get all the etudiants.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of etudiants in body
     */
    @RequestMapping(value = "/etudiants",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Etudiant> getAllEtudiants() {
        log.debug("REST request to get all Etudiants");
        List<Etudiant> etudiants; 
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) { 
          etudiants = etudiantRepository.findAll(); 
        }  
        else { 
          etudiants = new ArrayList<Etudiant>(); 
          etudiants.add(etudiantRepository.findAllForCurrentUser()); 
        } 
        return etudiants;
    }

    /**
     * GET  /etudiants/:id : get the "id" etudiant.
     *
     * @param id the id of the etudiant to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the etudiant, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/etudiants/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Etudiant> getEtudiant(@PathVariable Long id) {
        log.debug("REST request to get Etudiant : {}", id);
        Etudiant etudiant = etudiantRepository.findOne(id);
        return Optional.ofNullable(etudiant)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /etudiants/:id : delete the "id" etudiant.
     *
     * @param id the id of the etudiant to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/etudiants/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteEtudiant(@PathVariable Long id) {
        log.debug("REST request to delete Etudiant : {}", id);
        etudiantRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("etudiant", id.toString())).build();
    }
    /*@RequestMapping(value = "/etudiants/up/{id}/{affiliation}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
        @Timed
        public void updateEtudiantUp(@PathVariable Long id,@PathVariable String affiliation) throws URISyntaxException {
           
           Etudiant result = etudiantRepository.updateEtudiant(id, affiliation);
          
               etudiantRepository.save(result);
        }*/

    
    /**
     * GET  /etudiants/pdf : get the pdf for "all etudiant.
     *
     * @return a pdf
     */
    @RequestMapping(value = "/etudiants/pdf",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Timed
    public   ResponseEntity<InputStreamResource> getpdfEtudiant() throws Exception {
    	System.err.println("in EtudiantResource Rest PDF");
        log.debug("REST request to get Etudiant : {}");
            
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        	ByteArrayOutputStream file = new ByteArrayOutputStream(); 
//            OutputStream file = new BufferedOutputStream(args);//new FileOutputStream(new File("C:\\PDF_Java4s.pdf"));
	          Document document = new Document();
	          PdfWriter.getInstance(document, file);
	          
          //Inserting Image in PDF
		     Image image = Image.getInstance ("src/main/resources/2Firefox_wallpaper.png");
		     image.scaleAbsolute(100f, 140f);//image width,height	

		//Inserting Table in PDF
		     PdfPTable table=new PdfPTable(11);

                  PdfPCell title = new PdfPCell (new Paragraph ("Etudiants"));

                  //block pour le titre
			      title.setColspan (11);
			      title.setHorizontalAlignment (Element.ALIGN_CENTER);
			      title.setPadding (10.0f);
			      title.setBackgroundColor (new BaseColor (140, 221, 8));					               
			      table.addCell(title);
			       	 	 	 	 	 
			      //ajout des titre des collones
			      table.addCell("Id");
			      table.addCell("Nom");
			      table.addCell("Prenom");
			      table.addCell("Adresse");
			      table.addCell("Ntel");
			      table.addCell("Datenaissance");
			      table.addCell("Email");
			      table.addCell("Assurance");
			      table.addCell("Affiliation");
			      table.addCell("Sesan");
			      table.addCell("User");

			      //ajouts de tout le sétudiants
			      System.err.println("in etudiantRepository");
			      System.err.println("count : "+etudiantRepository.count());
			      List<Etudiant> etudiants; 
			          etudiants = etudiantRepository.findAll(); 
			          
			      for (int i = 0; i < etudiantRepository.count(); i++) {
			    	  Etudiant etudiant = etudiants.get(i);
			    	  if(etudiant.getId() != null)table.addCell(etudiant.getId().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getNom() != null)table.addCell(etudiant.getNom().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getPrenom() != null)table.addCell(etudiant.getPrenom().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getAdresse() != null)table.addCell(etudiant.getAdresse().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getNtel() != null)table.addCell(etudiant.getNtel().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getDatenaissance() != null)table.addCell(etudiant.getDatenaissance().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getEmail() != null)table.addCell(etudiant.getEmail().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getAssurance() != null)table.addCell(etudiant.getAssurance().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getAffiliation() != null)table.addCell(etudiant.getAffiliation().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getSesan() != null)table.addCell(etudiant.getSesan().toString());
			    	  else table.addCell("Nil");
			    	  if(etudiant.getUser() != null)table.addCell(etudiant.getUser().getId().toString());
			    	  else table.addCell("Nil");
				}
			      table.setSpacingBefore(30.0f);       // Space Before table starts, like margin-top in CSS
			      table.setSpacingAfter(30.0f);        // Space After table starts, like margin-Bottom in CSS								          

		 //Inserting List in PDF
//			      com.itextpdf.text.List list=new com.itextpdf.text.List(true,30);
//		          list.add(new ListItem("Item1"));
//			      list.add(new ListItem("Item2"));
//			      list.add(new ListItem("Some Thing..."));		

		 //Text formating in PDF
//               Chunk chunk=new Chunk("Hello ");
//				chunk.setUnderline(+1f,-2f);//1st co-ordinate is for line width,2nd is space between
//				Chunk chunk1=new Chunk("Yousra");
//				chunk1.setUnderline(+4f,-8f);
//				chunk1.setBackground(new BaseColor (17, 46, 193));      

		 //Now Insert Every Thing Into PDF Document
	         document.open();//PDF document opened........			       

//				document.add(image);

//				document.add(Chunk.NEWLINE);   //Something like in HTML :-)

               document.add(new Paragraph("Informations Etudiants"));
               document.add(new Paragraph("Document Generated On - "+new Date().toString()));	

				document.add(table);

//				document.add(chunk);
//				document.add(chunk1);

				document.add(Chunk.NEWLINE);   //Something like in HTML :-)							    

//  				document.newPage();            //Opened new page
//				document.add(list);            //In the new page we are going to add list

	         document.close();

		             file.close();

       System.out.println("Pdf created successfully..");
       
     
         InputStreamResource inputStreamResource = new InputStreamResource(new ByteArrayInputStream(file.toByteArray()));
         return ResponseEntity
               .ok()
               .contentLength(file.size())
               .contentType(
                       MediaType.parseMediaType("application/octet-stream"))
               .body(inputStreamResource);
    }
    
    /**
     * GET  /etudiant/pdf/:id : get the cvs for "id" etudiant.
     *
     * @param id the id list of the etudiants to retrieve
     * @return
     * @return tbd
     */
    @RequestMapping(value = "/etudiant/csv/{ids}",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public   ResponseEntity<Object> getcvsEtudiant(@PathVariable List<Long> ids) throws Exception {
    	System.err.println("in getpdfEtudiant Rest CVS" +ids);
    	log.debug("REST request to get etudiant : {}", ids);



    	Etudiant etudiant ;
    	try {
    		String txt = "";
    		txt += "Id,Nom,Prenom,Adresse,Ntel,Email,Assurance,Affiliation,Sesan,Datenaissance,User\r\n";
    		for (int i = 0; i < ids.size(); i++) {
    			etudiant = etudiantRepository.findOne(ids.get(i));
    			System.err.println("stage"+etudiant);
    			txt += etudiant.toStringArray();
			}
    		return ResponseEntity
    				.ok()
    				.contentLength(txt.length())
    				.contentType(MediaType.TEXT_PLAIN)
    				.body(txt);

    	} catch (Exception e) {
    		System.err.println("something went wrong while generating the cvs !");
    		e.printStackTrace();
    		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Error");
    	}
//    	return null;
    }
}
