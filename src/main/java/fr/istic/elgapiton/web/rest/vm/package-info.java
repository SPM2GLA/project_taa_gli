/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.istic.elgapiton.web.rest.vm;
