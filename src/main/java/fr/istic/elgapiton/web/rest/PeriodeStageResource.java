package fr.istic.elgapiton.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.istic.elgapiton.domain.PeriodeStage;
import fr.istic.elgapiton.domain.Stage;
import fr.istic.elgapiton.repository.PeriodeStageRepository;
import fr.istic.elgapiton.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing PeriodeStage.
 */
@RestController
@RequestMapping("/api")
public class PeriodeStageResource {

    private final Logger log = LoggerFactory.getLogger(PeriodeStageResource.class);
        
    @Inject
    private PeriodeStageRepository periodeStageRepository;

    /**
     * POST  /periode-stages : Create a new periodeStage.
     *
     * @param periodeStage the periodeStage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new periodeStage, or with status 400 (Bad Request) if the periodeStage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/periode-stages",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PeriodeStage> createPeriodeStage(@RequestBody PeriodeStage periodeStage) throws URISyntaxException {
        log.debug("REST request to save PeriodeStage : {}", periodeStage);
        if (periodeStage.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("periodeStage", "idexists", "A new periodeStage cannot already have an ID")).body(null);
        }
        PeriodeStage result = periodeStageRepository.save(periodeStage);
        return ResponseEntity.created(new URI("/api/periode-stages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("periodeStage", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /periode-stages : Updates an existing periodeStage.
     *
     * @param periodeStage the periodeStage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated periodeStage,
     * or with status 400 (Bad Request) if the periodeStage is not valid,
     * or with status 500 (Internal Server Error) if the periodeStage couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/periode-stages",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PeriodeStage> updatePeriodeStage(@RequestBody PeriodeStage periodeStage) throws URISyntaxException {
        log.debug("REST request to update PeriodeStage : {}", periodeStage);
        if (periodeStage.getId() == null) {
            return createPeriodeStage(periodeStage);
        }
        PeriodeStage result = periodeStageRepository.save(periodeStage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("periodeStage", periodeStage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /periode-stages : get all the periodeStages.
     *
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of periodeStages in body
     */
    @RequestMapping(value = "/periode-stages",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PeriodeStage> getAllPeriodeStages(@RequestParam(required = false) String filter) {
        if ("stage-is-null".equals(filter)) {
            log.debug("REST request to get all PeriodeStages where stage is null");
            return StreamSupport
                .stream(periodeStageRepository.findAll().spliterator(), false)
                .filter(periodeStage -> periodeStage.getStage() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all PeriodeStages");
        List<PeriodeStage> periodeStages = periodeStageRepository.findAll();
        return periodeStages;
    }

    /**
     * GET  /periode-stages/:id : get the "id" periodeStage.
     *
     * @param id the id of the periodeStage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the periodeStage, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/periode-stages/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PeriodeStage> getPeriodeStage(@PathVariable Long id) {
        log.debug("REST request to get PeriodeStage : {}", id);
        PeriodeStage periodeStage = periodeStageRepository.findOne(id);
        return Optional.ofNullable(periodeStage)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /periode-stages/:id : delete the "id" periodeStage.
     *
     * @param id the id of the periodeStage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/periode-stages/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePeriodeStage(@PathVariable Long id) {
        log.debug("REST request to delete PeriodeStage : {}", id);
        periodeStageRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("periodeStage", id.toString())).build();
    }

    /**
     * GET  /periode-stages/pdf/:id : get the cvs for "id" periode-stages.
     *
     * @param id the id list of the periode-stagess to retrieve
     * @return
     * @return tbd
     */
    @RequestMapping(value = "/periode-stage/csv/{ids}",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public   ResponseEntity<Object> getcvsPeriodeStage(@PathVariable List<Long> ids) throws Exception {
    	System.err.println("in getpdfPeriodeStage Rest CVS" +ids);
    	log.debug("REST request to get periodeStage : {}", ids);



    	PeriodeStage periodeStage ;
    	try {
    		String txt = "";
    		txt += "Id,Datedebut,Datefin\r\n";
    		for (int i = 0; i < ids.size(); i++) {
    			periodeStage = periodeStageRepository.findOne(ids.get(i));
    			System.err.println("stage"+periodeStage);
    			txt += periodeStage.toStringArray();
			}
    		return ResponseEntity
    				.ok()
    				.contentLength(txt.length())
    				.contentType(MediaType.TEXT_PLAIN)
    				.body(txt);

    	} catch (Exception e) {
    		System.err.println("something went wrong while generating the cvs !");
    		e.printStackTrace();
    		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Error");
    	}
//    	return null;
    }
}
