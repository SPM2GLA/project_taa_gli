package fr.istic.elgapiton.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.istic.elgapiton.domain.Activite;
import fr.istic.elgapiton.domain.Stage;
import fr.istic.elgapiton.repository.ActiviteRepository;
import fr.istic.elgapiton.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Activite.
 */
@RestController
@RequestMapping("/api")
public class ActiviteResource {

    private final Logger log = LoggerFactory.getLogger(ActiviteResource.class);
        
    @Inject
    private ActiviteRepository activiteRepository;

    /**
     * POST  /activites : Create a new activite.
     *
     * @param activite the activite to create
     * @return the ResponseEntity with status 201 (Created) and with body the new activite, or with status 400 (Bad Request) if the activite has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/activites",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Activite> createActivite(@RequestBody Activite activite) throws URISyntaxException {
        log.debug("REST request to save Activite : {}", activite);
        if (activite.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("activite", "idexists", "A new activite cannot already have an ID")).body(null);
        }
        Activite result = activiteRepository.save(activite);
        return ResponseEntity.created(new URI("/api/activites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("activite", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /activites : Updates an existing activite.
     *
     * @param activite the activite to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated activite,
     * or with status 400 (Bad Request) if the activite is not valid,
     * or with status 500 (Internal Server Error) if the activite couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/activites",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Activite> updateActivite(@RequestBody Activite activite) throws URISyntaxException {
        log.debug("REST request to update Activite : {}", activite);
        if (activite.getId() == null) {
            return createActivite(activite);
        }
        Activite result = activiteRepository.save(activite);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("activite", activite.getId().toString()))
            .body(result);
    }

    /**
     * GET  /activites : get all the activites.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of activites in body
     */
    @RequestMapping(value = "/activites",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Activite> getAllActivites() {
        log.debug("REST request to get all Activites");
        List<Activite> activites = activiteRepository.findAll();
        return activites;
    }

    /**
     * GET  /activites/:id : get the "id" activite.
     *
     * @param id the id of the activite to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the activite, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/activites/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Activite> getActivite(@PathVariable Long id) {
        log.debug("REST request to get Activite : {}", id);
        Activite activite = activiteRepository.findOne(id);
        return Optional.ofNullable(activite)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /activites/:id : delete the "id" activite.
     *
     * @param id the id of the activite to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/activites/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteActivite(@PathVariable Long id) {
        log.debug("REST request to delete Activite : {}", id);
        activiteRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("activite", id.toString())).build();
    }

    /**
     * GET  /activite/pdf/:id : get the cvs for "id" activite.
     *
     * @param id the id list of the activites to retrieve
     * @return
     * @return tbd
     */
    @RequestMapping(value = "/activite/csv/{ids}",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public   ResponseEntity<Object> getcvsActivite(@PathVariable List<Long> ids) throws Exception {
    	System.err.println("in getpdfActivite Rest CVS" +ids);
    	log.debug("REST request to get activite : {}", ids);



    	Activite activite ;
    	try {
    		String txt = "";
    		txt += "Id,Datedebut,Datefin\r\n";
    		for (int i = 0; i < ids.size(); i++) {
    			activite = activiteRepository.findOne(ids.get(i));
    			txt += activite.toStringArray();
			}
    		return ResponseEntity
    				.ok()
    				.contentLength(txt.length())
    				.contentType(MediaType.TEXT_PLAIN)
    				.body(txt);

    	} catch (Exception e) {
    		System.err.println("something went wrong while generating the cvs !");
    		e.printStackTrace();
    		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Error");
    	}
//    	return null;
    }
}
