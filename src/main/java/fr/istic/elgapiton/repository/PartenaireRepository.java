package fr.istic.elgapiton.repository;

import fr.istic.elgapiton.domain.Partenaire;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Partenaire entity.
 */
@SuppressWarnings("unused")
public interface PartenaireRepository extends JpaRepository<Partenaire,Long> {

    @Query("select distinct partenaire from Partenaire partenaire left join fetch partenaire.antivites")
    List<Partenaire> findAllWithEagerRelationships();

    @Query("select partenaire from Partenaire partenaire left join fetch partenaire.antivites where partenaire.id =:id")
    Partenaire findOneWithEagerRelationships(@Param("id") Long id);

}
