package fr.istic.elgapiton.repository;

import fr.istic.elgapiton.domain.DipIfsic;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the DipIfsic entity.
 */
@SuppressWarnings("unused")
public interface DipIfsicRepository extends JpaRepository<DipIfsic,Long> {

    @Query("select distinct dipIfsic from DipIfsic dipIfsic left join fetch dipIfsic.enseignants")
    List<DipIfsic> findAllWithEagerRelationships();

    @Query("select dipIfsic from DipIfsic dipIfsic left join fetch dipIfsic.enseignants where dipIfsic.id =:id")
    DipIfsic findOneWithEagerRelationships(@Param("id") Long id);

}
