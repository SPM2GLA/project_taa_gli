package fr.istic.elgapiton.repository;

import fr.istic.elgapiton.domain.DipIfsic; 
import fr.istic.elgapiton.domain.Etudiant;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param; 

import java.util.List;

/**
 * Spring Data JPA repository for the Etudiant entity.
 */
@SuppressWarnings("unused")
public interface EtudiantRepository extends JpaRepository<Etudiant,Long> {

	@Query("select etudiant from Etudiant etudiant where etudiant.user.id =:id") 
	Etudiant findOneByUserId(@Param("id") Long id);

	@Query("select etudiant from Etudiant etudiant where etudiant.user.login =?#{principal.username}") 
	Etudiant findAllForCurrentUser();
	
	//@Query("update etudiant set etudiant.affiliation=:affiliation where etudiant.user.id =:id") 
	//Etudiant updateEtudiant(@Param("id") Long id,@Param("affiliation") String affiliation);
}
