package fr.istic.elgapiton.repository;

import fr.istic.elgapiton.domain.Stage;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the Stage entity.
 */
@SuppressWarnings("unused")
public interface StageRepository extends JpaRepository<Stage,Long> {

    @Query("select distinct stage from Stage stage left join fetch stage.contacts")
    List<Stage> findAllWithEagerRelationships();

    @Query("select stage from Stage stage left join fetch stage.contacts where stage.id =:id")
    Stage findOneWithEagerRelationships(@Param("id") Long id);

    
    @Query("select distinct stage from Stage stage left join fetch stage.contacts where stage.etudiant.user.login =?#{principal.username}")
    List<Stage> findAllForCurrentUser();

 
    @Query("select distinct stage from Stage stage left join fetch stage.contacts where stage.sannee =:year")
    List<Stage> findAllForYear(@Param("year") LocalDate year);
    
   // @Query("select stage from Stage stage left join fetch stage.contacts where stage.partenaire.nom= :partner")
	//List<Stage> findAllWithPartner(@Param("partner") String partner);

}