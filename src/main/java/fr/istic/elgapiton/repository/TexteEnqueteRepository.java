package fr.istic.elgapiton.repository;

import fr.istic.elgapiton.domain.TexteEnquete;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TexteEnquete entity.
 */
@SuppressWarnings("unused")
public interface TexteEnqueteRepository extends JpaRepository<TexteEnquete,Long> {

}
