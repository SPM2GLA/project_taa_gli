package fr.istic.elgapiton.repository;

import fr.istic.elgapiton.domain.PeriodeStage;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PeriodeStage entity.
 */
@SuppressWarnings("unused")
public interface PeriodeStageRepository extends JpaRepository<PeriodeStage,Long> {

}
