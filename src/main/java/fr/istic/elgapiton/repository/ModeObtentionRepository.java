package fr.istic.elgapiton.repository;

import fr.istic.elgapiton.domain.ModeObtention;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ModeObtention entity.
 */
@SuppressWarnings("unused")
public interface ModeObtentionRepository extends JpaRepository<ModeObtention,Long> {

}
