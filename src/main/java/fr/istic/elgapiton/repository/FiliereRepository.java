package fr.istic.elgapiton.repository;

import fr.istic.elgapiton.domain.Filiere;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Filiere entity.
 */
@SuppressWarnings("unused")
public interface FiliereRepository extends JpaRepository<Filiere,Long> {

    @Query("select distinct filiere from Filiere filiere left join fetch filiere.enseignants")
    List<Filiere> findAllWithEagerRelationships();

    @Query("select filiere from Filiere filiere left join fetch filiere.enseignants where filiere.id =:id")
    Filiere findOneWithEagerRelationships(@Param("id") Long id);

}
