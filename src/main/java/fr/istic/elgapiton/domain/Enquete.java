package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Enquete.
 */
@Entity
@Table(name = "enquete")
public class Enquete implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "enquete_etudiant",
               joinColumns = @JoinColumn(name="enquetes_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="etudiants_id", referencedColumnName="ID"))
    private Set<Etudiant> etudiants = new HashSet<>();

    @ManyToOne
    private ModeObtention modeobtention;

    @OneToMany(mappedBy = "enquete")
    @JsonIgnore
    private Set<TexteEnquete> texteEnquetes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Etudiant> getEtudiants() {
        return etudiants;
    }

    public Enquete etudiants(Set<Etudiant> etudiants) {
        this.etudiants = etudiants;
        return this;
    }

    public Enquete addEtudiant(Etudiant etudiant) {
        etudiants.add(etudiant);
        etudiant.getEnquetes().add(this);
        return this;
    }

    public Enquete removeEtudiant(Etudiant etudiant) {
        etudiants.remove(etudiant);
        etudiant.getEnquetes().remove(this);
        return this;
    }

    public void setEtudiants(Set<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }

    public ModeObtention getModeobtention() {
        return modeobtention;
    }

    public Enquete modeobtention(ModeObtention modeObtention) {
        this.modeobtention = modeObtention;
        return this;
    }

    public void setModeobtention(ModeObtention modeObtention) {
        this.modeobtention = modeObtention;
    }

    public Set<TexteEnquete> getTexteEnquetes() {
        return texteEnquetes;
    }

    public Enquete texteEnquetes(Set<TexteEnquete> texteEnquetes) {
        this.texteEnquetes = texteEnquetes;
        return this;
    }

    public Enquete addTexteEnquete(TexteEnquete texteEnquete) {
        texteEnquetes.add(texteEnquete);
        texteEnquete.setEnquete(this);
        return this;
    }

    public Enquete removeTexteEnquete(TexteEnquete texteEnquete) {
        texteEnquetes.remove(texteEnquete);
        texteEnquete.setEnquete(null);
        return this;
    }

    public void setTexteEnquetes(Set<TexteEnquete> texteEnquetes) {
        this.texteEnquetes = texteEnquetes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Enquete enquete = (Enquete) o;
        if(enquete.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, enquete.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Enquete{" +
            "id=" + id +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		s+="\"";if(etudiants != null) for (Etudiant etudiant : etudiants) s+= etudiant.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\",";
		if(modeobtention != null) s+= modeobtention.getId(); else s += "Nil"; s+="\r\n";
		return s;
	}
}
