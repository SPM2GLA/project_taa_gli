package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Fonction.
 */
@Entity
@Table(name = "fonction")
public class Fonction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @OneToMany(mappedBy = "fonction")
    @JsonIgnore
    private Set<Stage> stages = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Fonction libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<Stage> getStages() {
        return stages;
    }

    public Fonction stages(Set<Stage> stages) {
        this.stages = stages;
        return this;
    }

    public Fonction addStage(Stage stage) {
        stages.add(stage);
        stage.setFonction(this);
        return this;
    }

    public Fonction removeStage(Stage stage) {
        stages.remove(stage);
        stage.setFonction(null);
        return this;
    }

    public void setStages(Set<Stage> stages) {
        this.stages = stages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Fonction fonction = (Fonction) o;
        if(fonction.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, fonction.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Fonction{" +
            "id=" + id +
            ", libelle='" + libelle + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(libelle != null) s+= "\""+libelle+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
//		s+="\"";if(stages != null) for (Stage stage : stages) s+= stage.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\"\r\n";
		return s;
	}
}
