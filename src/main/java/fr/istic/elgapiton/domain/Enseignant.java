package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Enseignant.
 */
@Entity
@Table(name = "enseignant")
public class Enseignant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "ntel")
    private String ntel;

    @OneToMany(mappedBy = "enseignant")
    @JsonIgnore
    private Set<Stage> stages = new HashSet<>();

    @ManyToMany(mappedBy = "enseignants")
    @JsonIgnore
    private Set<Filiere> filieres = new HashSet<>();

    @ManyToMany(mappedBy = "enseignants")
    @JsonIgnore
    private Set<DipIfsic> diplomeifsics = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Enseignant nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Enseignant prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public Enseignant adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNtel() {
        return ntel;
    }

    public Enseignant ntel(String ntel) {
        this.ntel = ntel;
        return this;
    }

    public void setNtel(String ntel) {
        this.ntel = ntel;
    }

    public Set<Stage> getStages() {
        return stages;
    }

    public Enseignant stages(Set<Stage> stages) {
        this.stages = stages;
        return this;
    }

    public Enseignant addStage(Stage stage) {
        stages.add(stage);
        stage.setEnseignant(this);
        return this;
    }

    public Enseignant removeStage(Stage stage) {
        stages.remove(stage);
        stage.setEnseignant(null);
        return this;
    }

    public void setStages(Set<Stage> stages) {
        this.stages = stages;
    }

    public Set<Filiere> getFilieres() {
        return filieres;
    }

    public Enseignant filieres(Set<Filiere> filieres) {
        this.filieres = filieres;
        return this;
    }

    public Enseignant addFiliere(Filiere filiere) {
        filieres.add(filiere);
        filiere.getEnseignants().add(this);
        return this;
    }

    public Enseignant removeFiliere(Filiere filiere) {
        filieres.remove(filiere);
        filiere.getEnseignants().remove(this);
        return this;
    }

    public void setFilieres(Set<Filiere> filieres) {
        this.filieres = filieres;
    }

    public Set<DipIfsic> getDiplomeifsics() {
        return diplomeifsics;
    }

    public Enseignant diplomeifsics(Set<DipIfsic> dipIfsics) {
        this.diplomeifsics = dipIfsics;
        return this;
    }

    public Enseignant addDiplomeifsic(DipIfsic dipIfsic) {
        diplomeifsics.add(dipIfsic);
        dipIfsic.getEnseignants().add(this);
        return this;
    }

    public Enseignant removeDiplomeifsic(DipIfsic dipIfsic) {
        diplomeifsics.remove(dipIfsic);
        dipIfsic.getEnseignants().remove(this);
        return this;
    }

    public void setDiplomeifsics(Set<DipIfsic> dipIfsics) {
        this.diplomeifsics = dipIfsics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Enseignant enseignant = (Enseignant) o;
        if(enseignant.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, enseignant.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Enseignant{" +
            "id=" + id +
            ", nom='" + nom + "'" +
            ", prenom='" + prenom + "'" +
            ", adresse='" + adresse + "'" +
            ", ntel='" + ntel + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(nom != null) s+= nom; else s += "Nil"; s+=",";
		if(prenom != null) s+= prenom; else s += "Nil"; s+=",";
		if(adresse != null) s+= "\""+adresse+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		if(ntel != null) s+= ntel+""; else s += "Nil"; s+=",";
		return s;
	}
}
