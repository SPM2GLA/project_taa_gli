package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Region.
 */
@Entity
@Table(name = "region")
public class Region implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "rlebelle", nullable = false)
    private String rlebelle;

    @OneToMany(mappedBy = "region")
    @JsonIgnore
    private Set<Partenaire> partenaires = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRlebelle() {
        return rlebelle;
    }

    public Region rlebelle(String rlebelle) {
        this.rlebelle = rlebelle;
        return this;
    }

    public void setRlebelle(String rlebelle) {
        this.rlebelle = rlebelle;
    }

    public Set<Partenaire> getPartenaires() {
        return partenaires;
    }

    public Region partenaires(Set<Partenaire> partenaires) {
        this.partenaires = partenaires;
        return this;
    }

    public Region addPartenaire(Partenaire partenaire) {
        partenaires.add(partenaire);
        partenaire.setRegion(this);
        return this;
    }

    public Region removePartenaire(Partenaire partenaire) {
        partenaires.remove(partenaire);
        partenaire.setRegion(null);
        return this;
    }

    public void setPartenaires(Set<Partenaire> partenaires) {
        this.partenaires = partenaires;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Region region = (Region) o;
        if(region.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, region.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Region{" +
            "id=" + id +
            ", rlebelle='" + rlebelle + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(rlebelle != null) s+= "\""+rlebelle+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		return s;
	}
}
