package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Filiere.
 */
@Entity
@Table(name = "filiere")
public class Filiere implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @OneToMany(mappedBy = "filiere")
    @JsonIgnore
    private Set<DipIfsic> diplomeifsics = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "filiere_enseignant",
               joinColumns = @JoinColumn(name="filieres_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="enseignants_id", referencedColumnName="ID"))
    private Set<Enseignant> enseignants = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Filiere libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<DipIfsic> getDiplomeifsics() {
        return diplomeifsics;
    }

    public Filiere diplomeifsics(Set<DipIfsic> dipIfsics) {
        this.diplomeifsics = dipIfsics;
        return this;
    }

    public Filiere addDiplomeifsic(DipIfsic dipIfsic) {
        diplomeifsics.add(dipIfsic);
        dipIfsic.setFiliere(this);
        return this;
    }

    public Filiere removeDiplomeifsic(DipIfsic dipIfsic) {
        diplomeifsics.remove(dipIfsic);
        dipIfsic.setFiliere(null);
        return this;
    }

    public void setDiplomeifsics(Set<DipIfsic> dipIfsics) {
        this.diplomeifsics = dipIfsics;
    }

    public Set<Enseignant> getEnseignants() {
        return enseignants;
    }

    public Filiere enseignants(Set<Enseignant> enseignants) {
        this.enseignants = enseignants;
        return this;
    }

    public Filiere addEnseignant(Enseignant enseignant) {
        enseignants.add(enseignant);
        enseignant.getFilieres().add(this);
        return this;
    }

    public Filiere removeEnseignant(Enseignant enseignant) {
        enseignants.remove(enseignant);
        enseignant.getFilieres().remove(this);
        return this;
    }

    public void setEnseignants(Set<Enseignant> enseignants) {
        this.enseignants = enseignants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Filiere filiere = (Filiere) o;
        if(filiere.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, filiere.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Filiere{" +
            "id=" + id +
            ", libelle='" + libelle + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(libelle != null) s+= "\""+libelle+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		s+="\"";if(enseignants != null) for (Enseignant enseignant : enseignants) s+= enseignant.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\"\r\n";
		return s;
	}
}
