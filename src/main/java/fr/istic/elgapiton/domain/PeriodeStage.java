package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PeriodeStage.
 */
@Entity
@Table(name = "periode_stage")
public class PeriodeStage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "datedebut")
    private LocalDate datedebut;

    @Column(name = "datefin")
    private LocalDate datefin;

    @OneToOne(mappedBy = "periodestage")
    @JsonIgnore
    private Stage stage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDatedebut() {
        return datedebut;
    }

    public PeriodeStage datedebut(LocalDate datedebut) {
        this.datedebut = datedebut;
        return this;
    }

    public void setDatedebut(LocalDate datedebut) {
        this.datedebut = datedebut;
    }

    public LocalDate getDatefin() {
        return datefin;
    }

    public PeriodeStage datefin(LocalDate datefin) {
        this.datefin = datefin;
        return this;
    }

    public void setDatefin(LocalDate datefin) {
        this.datefin = datefin;
    }

    public Stage getStage() {
        return stage;
    }

    public PeriodeStage stage(Stage stage) {
        this.stage = stage;
        return this;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PeriodeStage periodeStage = (PeriodeStage) o;
        if(periodeStage.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, periodeStage.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PeriodeStage{" +
            "id=" + id +
            ", datedebut='" + datedebut + "'" +
            ", datefin='" + datefin + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(datedebut != null) s+= datedebut.toString(); else s += "Nil"; s+=",";
		if(datefin != null) s+= datefin.toString(); else s += "Nil"; s+=",";
		return s;
	}
}
