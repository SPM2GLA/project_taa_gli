package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DipIfsic.
 */
@Entity
@Table(name = "dip_ifsic")
public class DipIfsic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @ManyToMany
    @JoinTable(name = "dip_ifsic_enseignant",
               joinColumns = @JoinColumn(name="dip_ifsics_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="enseignants_id", referencedColumnName="ID"))
    private Set<Enseignant> enseignants = new HashSet<>();

    @OneToOne(mappedBy = "diplomeifsic")
    @JsonIgnore
    private Stage stage;

    @ManyToOne
    private Filiere filiere;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public DipIfsic libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<Enseignant> getEnseignants() {
        return enseignants;
    }

    public DipIfsic enseignants(Set<Enseignant> enseignants) {
        this.enseignants = enseignants;
        return this;
    }

    public DipIfsic addEnseignant(Enseignant enseignant) {
        enseignants.add(enseignant);
        enseignant.getDiplomeifsics().add(this);
        return this;
    }

    public DipIfsic removeEnseignant(Enseignant enseignant) {
        enseignants.remove(enseignant);
        enseignant.getDiplomeifsics().remove(this);
        return this;
    }

    public void setEnseignants(Set<Enseignant> enseignants) {
        this.enseignants = enseignants;
    }

    public Stage getStage() {
        return stage;
    }

    public DipIfsic stage(Stage stage) {
        this.stage = stage;
        return this;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Filiere getFiliere() {
        return filiere;
    }

    public DipIfsic filiere(Filiere filiere) {
        this.filiere = filiere;
        return this;
    }

    public void setFiliere(Filiere filiere) {
        this.filiere = filiere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DipIfsic dipIfsic = (DipIfsic) o;
        if(dipIfsic.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, dipIfsic.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DipIfsic{" +
            "id=" + id +
            ", libelle='" + libelle + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(libelle != null) s+= "\""+libelle+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		s+="\"";if(enseignants != null) for (Enseignant enseignant : enseignants) s+= enseignant.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\",";
		if(filiere != null) s+= filiere; else s += "Nil"; s+="\r\n";
		return s;
	}
}
