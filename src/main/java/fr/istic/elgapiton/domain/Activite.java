package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Activite.
 */
@Entity
@Table(name = "activite")
public class Activite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "description")
    private String description;

    @ManyToMany(mappedBy = "antivites")
    @JsonIgnore
    private Set<Partenaire> partenaires = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Activite description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Partenaire> getPartenaires() {
        return partenaires;
    }

    public Activite partenaires(Set<Partenaire> partenaires) {
        this.partenaires = partenaires;
        return this;
    }

    public Activite addPartenaire(Partenaire partenaire) {
        partenaires.add(partenaire);
        partenaire.getAntivites().add(this);
        return this;
    }

    public Activite removePartenaire(Partenaire partenaire) {
        partenaires.remove(partenaire);
        partenaire.getAntivites().remove(this);
        return this;
    }

    public void setPartenaires(Set<Partenaire> partenaires) {
        this.partenaires = partenaires;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Activite activite = (Activite) o;
        if(activite.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, activite.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Activite{" +
            "id=" + id +
            ", description='" + description + "'" +
            '}';
    }

    public String toStringArray() {
  		String s ="";
  		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
  		if(description != null) s+= "\""+description+"\""; else s += "Nil"; s+="\r\n"; // pensé a "\ les chaines de caractères
  		return s;
  	}
}
