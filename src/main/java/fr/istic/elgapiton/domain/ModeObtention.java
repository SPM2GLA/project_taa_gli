package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ModeObtention.
 */
@Entity
@Table(name = "mode_obtention")
public class ModeObtention implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "obtdescription")
    private String obtdescription;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER,mappedBy = "modeobtention")
    @JsonIgnore
    private Set<Enquete> enquetes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObtdescription() {
        return obtdescription;
    }

    public ModeObtention obtdescription(String obtdescription) {
        this.obtdescription = obtdescription;
        return this;
    }

    public void setObtdescription(String obtdescription) {
        this.obtdescription = obtdescription;
    }

    public Set<Enquete> getEnquetes() {
        return enquetes;
    }

    public ModeObtention enquetes(Set<Enquete> enquetes) {
        this.enquetes = enquetes;
        return this;
    }

    public ModeObtention addEnquete(Enquete enquete) {
        enquetes.add(enquete);
        enquete.setModeobtention(this);
        return this;
    }

    public ModeObtention removeEnquete(Enquete enquete) {
        enquetes.remove(enquete);
        enquete.setModeobtention(null);
        return this;
    }

    public void setEnquetes(Set<Enquete> enquetes) {
        this.enquetes = enquetes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModeObtention modeObtention = (ModeObtention) o;
        if(modeObtention.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, modeObtention.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ModeObtention{" +
            "id=" + id +
            ", obtdescription='" + obtdescription + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(obtdescription != null) s+= "\""+obtdescription+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		s+="\"";if(enquetes != null) for (Enquete enquete : enquetes) s+= enquete.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\"\r\n";
		return s;
	}
}
