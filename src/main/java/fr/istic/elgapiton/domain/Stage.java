package fr.istic.elgapiton.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Stage.
 */
@Entity
@Table(name = "stage")
public class Stage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "sujet")
    private String sujet;

    @Column(name = "salaire")
    private Integer salaire;

    @Column(name = "sannee")
    private LocalDate sannee;

    
    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(unique = true)
    private DipIfsic diplomeifsic;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(unique = true)
    private PeriodeStage periodestage;


    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "stage_contact",
               joinColumns = @JoinColumn(name="stages_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="contacts_id", referencedColumnName="ID"))
    private Set<Contact> contacts = new HashSet<>();

    @ManyToOne
    private Etudiant etudiant;

    @ManyToOne
    private Partenaire partenaire;

    @ManyToOne
    private Enseignant enseignant;

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Fonction fonction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSujet() {
        return sujet;
    }

    public Stage sujet(String sujet) {
        this.sujet = sujet;
        return this;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public Integer getSalaire() {
        return salaire;
    }

    public Stage salaire(Integer salaire) {
        this.salaire = salaire;
        return this;
    }

    public void setSalaire(Integer salaire) {
        this.salaire = salaire;
    }

    public LocalDate getSannee() {
        return sannee;
    }

    public Stage sannee(LocalDate sannee) {
        this.sannee = sannee;
        return this;
    }

    public void setSannee(LocalDate sannee) {
        this.sannee = sannee;
    }

    public DipIfsic getDiplomeifsic() {
        return diplomeifsic;
    }

    public Stage diplomeifsic(DipIfsic dipIfsic) {
        this.diplomeifsic = dipIfsic;
        return this;
    }

    public void setDiplomeifsic(DipIfsic dipIfsic) {
        this.diplomeifsic = dipIfsic;
    }

    public PeriodeStage getPeriodestage() {
        return periodestage;
    }

    public Stage periodestage(PeriodeStage periodeStage) {
        this.periodestage = periodeStage;
        return this;
    }

    public void setPeriodestage(PeriodeStage periodeStage) {
        this.periodestage = periodeStage;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public Stage contacts(Set<Contact> contacts) {
        this.contacts = contacts;
        return this;
    }

    public Stage addContact(Contact contact) {
        contacts.add(contact);
        contact.getStages().add(this);
        return this;
    }

    public Stage removeContact(Contact contact) {
        contacts.remove(contact);
        contact.getStages().remove(this);
        return this;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public Stage etudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
        return this;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public Partenaire getPartenaire() {
        return partenaire;
    }

    public Stage partenaire(Partenaire partenaire) {
        this.partenaire = partenaire;
        return this;
    }

    public void setPartenaire(Partenaire partenaire) {
        this.partenaire = partenaire;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public Stage enseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
        return this;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public Fonction getFonction() {
        return fonction;
    }

    public Stage fonction(Fonction fonction) {
        this.fonction = fonction;
        return this;
    }

    public void setFonction(Fonction fonction) {
        this.fonction = fonction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Stage stage = (Stage) o;
        if(stage.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, stage.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Stage{" +
            "id=" + id +
            ", sujet='" + sujet + "'" +
            ", salaire='" + salaire + "'" +
            ", sannee='" + sannee + "'" +
            '}';
    }

	public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(sujet != null) s+= "\""+sujet+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		if(salaire != null) s+= salaire.toString(); else s += "Nil"; s+=",";
		if(sannee != null) s+= sannee.getYear()+""; else s += "Nil"; s+=",";
		if(diplomeifsic != null) s+= diplomeifsic.getId(); else s += "Nil"; s+=",";
		if(periodestage != null) s+= periodestage.getId(); else s += "Nil"; s+=",";
		s+="\"";if(contacts != null) for (Contact contact : contacts) s+= contact.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\",";
		if(etudiant != null) s+= etudiant.getId(); else s += "Nil"; s+=",";
		if(partenaire != null) s+= partenaire.getId(); else s += "Nil"; s+=",";
		if(enseignant != null) s+= enseignant.getId(); else s += "Nil"; s+=",";
		if(fonction != null) s+= fonction.getId(); else s += "Nil"; s+="\r\n";
		return s;
	}
}
