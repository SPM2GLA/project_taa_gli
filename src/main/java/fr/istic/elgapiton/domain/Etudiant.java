package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Etudiant.
 */
@Entity
@Table(name = "etudiant")
public class Etudiant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "ntel")
    private String ntel;

    @Column(name = "email")
    private String email;

    @Column(name = "assurance")
    private String assurance;

    @Column(name = "affiliation")
    private String affiliation;

    @Column(name = "sesan")
    private Long sesan;

    @Column(name = "datenaissance")
    private LocalDate datenaissance;

    @OneToMany(mappedBy = "etudiant")
    @JsonIgnore
    private Set<Stage> stages = new HashSet<>();

    @ManyToMany(mappedBy = "etudiants")
    @JsonIgnore
    private Set<Enquete> enquetes = new HashSet<>();

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Etudiant nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Etudiant prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public Etudiant adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNtel() {
        return ntel;
    }

    public Etudiant ntel(String ntel) {
        this.ntel = ntel;
        return this;
    }

    public void setNtel(String ntel) {
        this.ntel = ntel;
    }

    public String getEmail() {
        return email;
    }

    public Etudiant email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAssurance() {
        return assurance;
    }

    public Etudiant assurance(String assurance) {
        this.assurance = assurance;
        return this;
    }

    public void setAssurance(String assurance) {
        this.assurance = assurance;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public Etudiant affiliation(String affiliation) {
        this.affiliation = affiliation;
        return this;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public Long getSesan() {
        return sesan;
    }

    public Etudiant sesan(Long sesan) {
        this.sesan = sesan;
        return this;
    }

    public void setSesan(Long sesan) {
        this.sesan = sesan;
    }

    public LocalDate getDatenaissance() {
        return datenaissance;
    }

    public Etudiant datenaissance(LocalDate datenaissance) {
        this.datenaissance = datenaissance;
        return this;
    }

    public void setDatenaissance(LocalDate datenaissance) {
        this.datenaissance = datenaissance;
    }

    public Set<Stage> getStages() {
        return stages;
    }

    public Etudiant stages(Set<Stage> stages) {
        this.stages = stages;
        return this;
    }

    public Etudiant addStage(Stage stage) {
        stages.add(stage);
        stage.setEtudiant(this);
        return this;
    }

    public Etudiant removeStage(Stage stage) {
        stages.remove(stage);
        stage.setEtudiant(null);
        return this;
    }

    public void setStages(Set<Stage> stages) {
        this.stages = stages;
    }

    public Set<Enquete> getEnquetes() {
        return enquetes;
    }

    public Etudiant enquetes(Set<Enquete> enquetes) {
        this.enquetes = enquetes;
        return this;
    }

    public Etudiant addEnquete(Enquete enquete) {
        enquetes.add(enquete);
        enquete.getEtudiants().add(this);
        return this;
    }

    public Etudiant removeEnquete(Enquete enquete) {
        enquetes.remove(enquete);
        enquete.getEtudiants().remove(this);
        return this;
    }

    public void setEnquetes(Set<Enquete> enquetes) {
        this.enquetes = enquetes;
    }

    public User getUser() {
        return user;
    }

    public Etudiant user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Etudiant etudiant = (Etudiant) o;
        if(etudiant.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, etudiant.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Etudiant{" +
            "id=" + id +
            ", nom='" + nom + "'" +
            ", prenom='" + prenom + "'" +
            ", adresse='" + adresse + "'" +
            ", ntel='" + ntel + "'" +
            ", email='" + email + "'" +
            ", assurance='" + assurance + "'" +
            ", affiliation='" + affiliation + "'" +
            ", sesan='" + sesan + "'" +
            ", datenaissance='" + datenaissance + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(nom != null) s+= nom+""; else s += "Nil"; s+=",";
		if(prenom != null) s+= prenom+""; else s += "Nil"; s+=",";
		if(adresse != null) s+= "\""+adresse+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		if(ntel != null) s+= ntel+""; else s += "Nil"; s+=",";
		if(email != null) s+= email; else s += "Nil"; s+=",";
		if(assurance != null) s+= "\""+assurance+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		if(affiliation != null) s+= "\""+affiliation+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		if(sesan != null) s+= sesan; else s += "Nil"; s+=",";
		if(datenaissance != null) s+= datenaissance.toString(); else s += "Nil"; s+="\r\n";
		return s;
	}
}
