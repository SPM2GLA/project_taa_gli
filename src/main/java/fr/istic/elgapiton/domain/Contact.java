package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Contact.
 */
@Entity
@Table(name = "contact")
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "ntel")
    private Integer ntel;

    @Column(name = "email")
    private String email;

    @Column(name = "sex")
    private String sex;

    @ManyToMany
    @JoinTable(name = "contact_partenaire",
               joinColumns = @JoinColumn(name="contacts_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="partenaires_id", referencedColumnName="ID"))
    private Set<Partenaire> partenaires = new HashSet<>();

    @ManyToMany(mappedBy = "contacts")
    @JsonIgnore
    private Set<Stage> stages = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Contact nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getNtel() {
        return ntel;
    }

    public Contact ntel(Integer ntel) {
        this.ntel = ntel;
        return this;
    }

    public void setNtel(Integer ntel) {
        this.ntel = ntel;
    }

    public String getEmail() {
        return email;
    }

    public Contact email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public Contact sex(String sex) {
        this.sex = sex;
        return this;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Set<Partenaire> getPartenaires() {
        return partenaires;
    }

    public Contact partenaires(Set<Partenaire> partenaires) {
        this.partenaires = partenaires;
        return this;
    }

    public Contact addPartenaire(Partenaire partenaire) {
        partenaires.add(partenaire);
        partenaire.getContacts().add(this);
        return this;
    }

    public Contact removePartenaire(Partenaire partenaire) {
        partenaires.remove(partenaire);
        partenaire.getContacts().remove(this);
        return this;
    }

    public void setPartenaires(Set<Partenaire> partenaires) {
        this.partenaires = partenaires;
    }

    public Set<Stage> getStages() {
        return stages;
    }

    public Contact stages(Set<Stage> stages) {
        this.stages = stages;
        return this;
    }

    public Contact addStage(Stage stage) {
        stages.add(stage);
        stage.getContacts().add(this);
        return this;
    }

    public Contact removeStage(Stage stage) {
        stages.remove(stage);
        stage.getContacts().remove(this);
        return this;
    }

    public void setStages(Set<Stage> stages) {
        this.stages = stages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contact contact = (Contact) o;
        if(contact.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, contact.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Contact{" +
            "id=" + id +
            ", nom='" + nom + "'" +
            ", ntel='" + ntel + "'" +
            ", email='" + email + "'" +
            ", sex='" + sex + "'" +
            '}';
    }

    public String toStringArray() {
  		String s ="";
  		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
  		if(nom != null) s+= "\""+nom+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
  		if(ntel != null) s+= ""+ntel; else s += "Nil"; s+=",";
        if(email != null) s+= email; else s += "Nil"; s+=",";
  		if(sex != null) s+= sex; else s += "Nil"; s+=",";
        s+="\"";if(partenaires != null) for (Partenaire partenaire : partenaires) s+= partenaire.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\"\r\n";
  		return s;
  	}
}
