package fr.istic.elgapiton.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Partenaire.
 */
@Entity
@Table(name = "partenaire")
public class Partenaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "siret")
    private Long siret;

    @Column(name = "datecreation")
    private LocalDate datecreation;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER,mappedBy = "partenaire")
    @JsonIgnore
    private Set<Stage> stages = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "partenaire_antivite",
               joinColumns = @JoinColumn(name="partenaires_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="antivites_id", referencedColumnName="ID"))
    private Set<Activite> antivites = new HashSet<>();

    @ManyToOne
    private Region region;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, mappedBy = "partenaires")
    @JsonIgnore
    private Set<Contact> contacts = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Partenaire nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public Partenaire adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Long getSiret() {
        return siret;
    }

    public Partenaire siret(Long siret) {
        this.siret = siret;
        return this;
    }

    public void setSiret(Long siret) {
        this.siret = siret;
    }

    public LocalDate getDatecreation() {
        return datecreation;
    }

    public Partenaire datecreation(LocalDate datecreation) {
        this.datecreation = datecreation;
        return this;
    }

    public void setDatecreation(LocalDate datecreation) {
        this.datecreation = datecreation;
    }

    public Set<Stage> getStages() {
        return stages;
    }

    public Partenaire stages(Set<Stage> stages) {
        this.stages = stages;
        return this;
    }

    public Partenaire addStage(Stage stage) {
        stages.add(stage);
        stage.setPartenaire(this);
        return this;
    }

    public Partenaire removeStage(Stage stage) {
        stages.remove(stage);
        stage.setPartenaire(null);
        return this;
    }

    public void setStages(Set<Stage> stages) {
        this.stages = stages;
    }

    public Set<Activite> getAntivites() {
        return antivites;
    }

    public Partenaire antivites(Set<Activite> activites) {
        this.antivites = activites;
        return this;
    }

    public Partenaire addAntivite(Activite activite) {
        antivites.add(activite);
        activite.getPartenaires().add(this);
        return this;
    }

    public Partenaire removeAntivite(Activite activite) {
        antivites.remove(activite);
        activite.getPartenaires().remove(this);
        return this;
    }

    public void setAntivites(Set<Activite> activites) {
        this.antivites = activites;
    }

    public Region getRegion() {
        return region;
    }

    public Partenaire region(Region region) {
        this.region = region;
        return this;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public Partenaire contacts(Set<Contact> contacts) {
        this.contacts = contacts;
        return this;
    }

    public Partenaire addContact(Contact contact) {
        contacts.add(contact);
        contact.getPartenaires().add(this);
        return this;
    }

    public Partenaire removeContact(Contact contact) {
        contacts.remove(contact);
        contact.getPartenaires().remove(this);
        return this;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Partenaire partenaire = (Partenaire) o;
        if(partenaire.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, partenaire.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Partenaire{" +
            "id=" + id +
            ", nom='" + nom + "'" +
            ", adresse='" + adresse + "'" +
            ", datecreation='" + datecreation + "'" +
            ", siret='" + siret + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(nom != null) s+= nom.toString(); else s += "Nil"; s+=",";
		if(adresse != null) s+= "\""+adresse+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		if(datecreation != null) s+= datecreation.toString(); else s += "Nil"; s+=",";
		if(siret != null) s+= ""+siret; else s += "Nil"; s+=",";
		s+="\"";if(antivites != null) for (Activite antivite : antivites) s+= antivite.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\",";
		if(region != null) s+= region.getId(); else s += "Nil"; s+=",";
		s+="\"";if(stages != null) for (Stage stage : stages) s+= stage.getId()+","; else s += "Nil,"; s=s.substring(0, s.length() - 1); s+="\"\r\n";
		return s;
	}
}
