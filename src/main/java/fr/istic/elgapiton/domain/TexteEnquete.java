package fr.istic.elgapiton.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TexteEnquete.
 */
@Entity
@Table(name = "texte_enquete")
public class TexteEnquete implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "reponse")
    private String reponse;

    @Column(name = "question")
    private String question;

    @Column(name = "dureerecherche")
    private Long dureerecherche;

    @Column(name = "salaire")
    private Long salaire;

    @ManyToOne
    private Enquete enquete;

    @ManyToOne
    private Etudiant etudiant;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReponse() {
        return reponse;
    }

    public TexteEnquete reponse(String reponse) {
        this.reponse = reponse;
        return this;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public String getQuestion() {
        return question;
    }

    public TexteEnquete question(String question) {
        this.question = question;
        return this;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Long getDureerecherche() {
        return dureerecherche;
    }

    public TexteEnquete dureerecherche(Long dureerecherche) {
        this.dureerecherche = dureerecherche;
        return this;
    }

    public void setDureerecherche(Long dureerecherche) {
        this.dureerecherche = dureerecherche;
    }

    public Long getSalaire() {
        return salaire;
    }

    public TexteEnquete salaire(Long salaire) {
        this.salaire = salaire;
        return this;
    }

    public void setSalaire(Long salaire) {
        this.salaire = salaire;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public TexteEnquete etudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
        return this;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public Enquete getEnquete() {
        return enquete;
    }

    public TexteEnquete enquete(Enquete enquete) {
        this.enquete = enquete;
        return this;
    }

    public void setEnquete(Enquete enquete) {
        this.enquete = enquete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TexteEnquete texteEnquete = (TexteEnquete) o;
        if(texteEnquete.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, texteEnquete.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TexteEnquete{" +
            "id=" + id +
            ", reponse='" + reponse + "'" +
            ", question='" + question + "'" +
            ", dureerecherche='" + dureerecherche + "'" +
            ", salaire='" + salaire + "'" +
            '}';
    }
    
    public String toStringArray() {
		String s ="";
		if(id != null) s+= id.toString(); else s += "Nil"; s+=",";
		if(reponse != null) s+= "\""+reponse+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		if(question != null) s+= "\""+question+"\""; else s += "Nil"; s+=","; // pensé a "\ les chaines de caractères
		if(dureerecherche != null) s+= ""+dureerecherche.toString(); else s += "Nil"; s+=","; 
	    if(salaire != null) s+= ""+salaire+""; else s += "Nil"; s+=",";
		if(etudiant != null) s+= etudiant.getId(); else s += "Nil"; s+=",";
	    if(enquete != null) s+= ""+enquete.getId()+""; else s += "Nil"; s+="\r\n";
		return s;
	}
}
