# projet TAA/GLI Yousra El Ghzizal & Stephen Piton
Un pdf **"Rapport Projet taa gli.pdf"** est disponible !
## Architecture en couche

 ![Architecture en couche](Rapport/Img1.png)

L'architecture utilis�e est une architecture 3-tiers (pr�sentation, m�tier, persistance), utilisant principalement les technologies Spring, AngularJS et JPA. Le projet a �t� g�n�r� gr�ce � Jhipster qui permet un d�ploiement rapide gr�ce � l'utilisation de frameworks tels que Springboot, Bower et Maven.

Pr�sentation des donn�es : 
* C�t� navigateur, utilise les technologies suivantes :
  * HTML5 Boilerplate 
  * Twitter Bootstrap 
  * AngularJS
  * Angular Translate
	
Traitement m�tier des donn�es : 
* C�t� serveur JAVA, utilise les technologies suivantes :
  * Spring Boot
  * Maven 
  * Spring MVC REST + Jackson 
  * Spring Security 
  * Itext pour g�n�rer des pdf

Persistances : 
* C�t� serveur JAVA, permet de g�rer l'acc�s � la base de donn�es MySQL :
  * Spring Data JPA
  * Liquibase 

 En plus des modules propos�s directement par Jhipster lors de la cr�ation du projet, nous avons utilis� la librairie itext, elle nous permet de g�n�rer un pdf en m�moire et de le renvoyer gr�ce au service RESTful � l'utilisateur. 
 
## Architecture des packages de l'application :
 
 ![diagramme de package](Rapport/Img2.png)

L'application comprend deux packages principaux, la partie serveur dans src/main/java et la partie client dans src/main/webapp.

Pour la partie serveur, on retrouve les packages principaux suivant :
* Spring Aop, pour faire le logging
* Domain, qui contient nos entit�s m�tiers (cf Illustration 3)
* Repository, pour la persistance des donn�es
* RESTful, pour la communication entre la partie client et la partie serveur

	C'est sur ces diff�rents packages que nous avons travaill� pour r�aliser notre applications.

Pour la partie client, on retrouve dans le dossier app une arborescence cr��e automatiquement lors de la g�n�ration du projet, avec notamment les dossier suivants :
* Entities, toutes les interfaces g�n�r�es automatiquement permettant de cr�er et d'interagir avec les diff�rentes entit�s m�tier cr��es.
* Convention, notre interface permettant � un utilisateur de cr�er une convention � partir de 0.
* Enquetes, notre interface de cr�ation et d'interaction avec les �tudiants.
* Mailingpartners, notre interface de filtrage permettant de trouver les diff�rentes personnes en charge des stages.
* Un certain nombre d'autres dossiers contenant nos interfaces d'administration.

## Mod�le m�tier 

![Entit�s m�tier telles que pr�sentes dans la version finale.
g�n�r� avec JDL studio.](Rapport/Img0.png)

Notre mod�le m�tier reprend une partie des classes pr�sentes sur l'application d'origine.
Pour l'int�gration avec les classes m�tier g�n�r�es lors de la cr�ation de l'application nous avons cr�� une relation OneToOne entre un Etudiant et un User, un user �tant l'entit� utilis�e pour la partie s�curit� et permettant � un utilisateur de s'authentifier.  Cette relation permet de faire la distinction entre un administrateur du syst�me qui a acc�s � toutes la partie cr�ation d'�tudiant, mailing, enqu�tes et administration des donn�es, et les �tudiants qui peuvent uniquement modifier leurs propres informations ainsi que cr�� une convention de stage et l'exporter en pdf.



