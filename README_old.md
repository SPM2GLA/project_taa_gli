# TODOLIST

x	que faire de l'age de l'etudiant
x		supression

x	reparer les partenaires
x	l'enregistrement des stages, ou l'affihages des stages ne fonctionne pas
x		déplacer les nouvelles methode du controleur vers la conventions
		
	
	les données de l'etudiant ne sont pas figé dans le temps
		peut etre crée un nouvelle etudiant(copie des données) et changer l'utilisateur associé au nouvel etudiant, transparent pour l'utilisateur, rechercher le stage avec une donnée commune au lieu de l'utilisateur, ex sesam
		
	autoriser les modifications du stage tant qu'ils ne sont pas validé par un admin
		après uniquement affichage
		ajouter un boolean validation au stage
			aucune modification, default false, si true griser le bouton modification pour l'etudiant
	déplacer duréeRecherche & Salaire vers Texteenquete ( 1 texteenquete par étudiant)
			1 enquete sur plusieurs étudiants, chaque etudiant a un texte enquete donc une liste de texte enquete
			
			Enquete :
				Modeobtention String
				Etudiant Many to Many
				Texteenquete One to Many
			TexteEnquete : ( pour 1 etudiant )
				Salaire Integer
				Dureerecherche Integer en semaine
				Reponse String
				Questions String
				Etudiant One to One
				
x	changer :
x		c'est l'admin qui crée les étudiants avec juste nom.prénom
		
		
x	rajout à Etudiant :
x		email
x		Affiliation sécu (String)
x		Caisse d'assurance Maladie (String)
		
x	période de stage :
x		récupérer les 2 dates dans stages lors de l'ajout
		
x	Fonction :
x		changer de Etudiant à Stage (possible d'avoir une fonction différente à chaque stage)
x		pouvoir sélectionner une même fonction pour plusieurs étudiants 
		
	bloquer l'accès à un menu pour l'admin (USER&ADMIN) mais pas pour que les users

x	Question et reponse dans texte enquete

x	faire un plan pour la convention

	-verifier le format et la validité de l'adresse mail, des nombres : afficher une erreur si l'utilisateur par exemple ne saisi pas un email qui respecte le format d'un email, et aussi afficher une erreur si il saisi un caractère dans un champ réservé à un entrier,vérifier les champs des nombres:quelques champs contiennent un select ....
	
	verification de numero de tel
		Contacts.ntel
		Etudiants.ntel
		enseignant.Ntel
	verification email
		Contacts.Email
		etudiant.Email
	verification date
		etudiant.Datenaissance
		stage.Sannee
		PeriodeStages.Datedebut
		PeriodeStages.Datefin
		partenaire.datecreation
x	-Ajouter un numero d'étudiant pour l'entity etudiant 
x	-Ajouter un numero Siret de partenaire pour faire le filtre

	filtres des données avancée :
		recherche les étudiant, dans une entreprise spécifique, avec des contacts, de tel année

x	vérifier la disponibilité des donnée avec les API rest
x		tester la possibilité de sortir toutes les relation d'un étudiants
x		sortir un pdf simple

	faire un formulaire de création d'une convention
		crée ou sélectionner à chaque étape une entitée ou un paramètres
			crée une nouvelle entrée si elle n'existe pas ou n'est pas a jours

x	un étudiant est un user
x		faire le lien entre les 2 class (avec un OnetoOne)
		gérer supression utilisateur/login et casser le lien dans étudiant
		
x	restreindre les ation des user/admin
x		choisir quel données afficher en fonction du role
	
x	gestion des donnée figer dans le temps
x		comment faire le lien entre plusieurs entrepriser avec une adresse différente
x		actuellement plusieurs etrées seront crée lors de la modifications de chaques données
x			ex : un nouveau partenaire pour un changement d'adresse


	Faire des tests qui fonctionne



# Docker

Start MySql :

    docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:latest

si déjà crée :


	Error response from daemon: Conflict. The name "mysql" is already in use by container f2e21ff07e69. You have to remove (or rename) that container to be able to reuse that name.

	docker start f2e21ff07e69

Crée la base avec MySQL workbench par exemple :  

	create database project_TAA_GLI;


Pour forcer le merge de stephen dans master :

	checkout master
	merge origin/stephen  -X theirs

rajout manuel des entity en cas de problèmes avec l'importation jdl :

	yo jhipster:import-jdl jhipster-jdl.jh
	yo jhipster --force --with-entities
	mvn liquibase:update

si problem de checksum liquibase :

	mvn liquibase:clearCheckSums

# project_TAA_GLI

This application was generated using JHipster, you can find documentation and help at [https://jhipster.github.io](https://jhipster.github.io).

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools (like
[Bower][] and [BrowserSync][]). You will only need to run this command when dependencies change in package.json.

    npm install

We use [Gulp][] as our build system. Install the Gulp command-line tool globally with:

    npm install -g gulp

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw
    gulp

Bower is used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in `bower.json`. You can also run `bower update` and `bower install` to manage dependencies.
Add the `-h` flag on any command to see how you can use it. For example, `bower update -h`.


## Building for production

To optimize the project_TAA_GLI client for production, run:

    ./mvnw -Pprod clean package

This will concatenate and minify CSS and JavaScript files. It will also modify `index.html` so it references
these new files.

To ensure everything worked, run:

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

## Testing

Unit tests are run by [Karma][] and written with [Jasmine][]. They're located in `src/test/javascript/` and can be run with:

    gulp test



## Continuous Integration

To setup this project in Jenkins, use the following configuration:

* Project name: `project_TAA_GLI`
* Source Code Management
    * Git Repository: `git@github.com:xxxx/project_TAA_GLI.git`
    * Branches to build: `*/master`
    * Additional Behaviours: `Wipe out repository & force clone`
* Build Triggers
    * Poll SCM / Schedule: `H/5 * * * *`
* Build
    * Invoke Maven / Tasks: `-Pprod clean package`
* Post-build Actions
    * Publish JUnit test result report / Test Report XMLs: `build/test-results/*.xml`

[JHipster]: https://jhipster.github.io/
[Gatling]: http://gatling.io/
[Node.js]: https://nodejs.org/
[Bower]: http://bower.io/
[Gulp]: http://gulpjs.com/
[BrowserSync]: http://www.browsersync.io/
[Karma]: http://karma-runner.github.io/
[Jasmine]: http://jasmine.github.io/2.0/introduction.html
[Protractor]: https://angular.github.io/protractor/
